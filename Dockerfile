FROM openjdk:19-jdk-alpine3.16 as builder
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
RUN java -Djarmode=layertools --enable-preview -jar app.jar extract

FROM openjdk:19-jdk-alpine3.16
RUN addgroup --system app && adduser --system --ingroup app app

USER app
VOLUME /tmp
WORKDIR /app

EXPOSE 8080

COPY --from=builder dependencies/ ./
COPY --from=builder snapshot-dependencies/ ./
COPY --from=builder spring-boot-loader/ ./
COPY --from=builder application/ ./




ENTRYPOINT ["java", "org.springframework.boot.loader.JarLauncher"]
