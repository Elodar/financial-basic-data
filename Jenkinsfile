def podTemplate = """
apiVersion: v1
kind: Pod
metadata:
  namespace: devops-ci
  name: financial-basic-data-ci
spec:
  containers:
    - name: maven
      image: maven:3.8.6-eclipse-temurin-17-alpine
      imagePullPolicy: IfNotPresent
      command:
        - cat
      tty: true
      resources:
        requests:
          cpu: "500m"
    - name: kaniko
      image: nexus.lppdev.pl:8088/kaniko-project/executor:v1.9.1-debug 
      command:
        - cat
      tty: true
      securityContext:
        privileged: true
      resources:
        requests:
          cpu: "500m"
          memory: "1Gi"
        limits:
          memory: "2Gi"
      env:
        - name: JENKINS_AGENT_WORKDIR
          value: /home/jenkins/agent
        - name: http_proxy
          value: "10.0.1.55:9999"
      volumeMounts:
        - mountPath: /var/lib/docker
          name: libcontainers
        - mountPath: /kaniko/.docker
          name: kaniko-secret
    - name: git
      image: nexus.lppdev.pl:8084/ci-tools:latest
      imagePullPolicy: Always
      tty: true
      command:
        - cat
  serviceAccount: jenkins
  nodeSelector:
    beta.kubernetes.io/os: linux
  tolerations:
    - key: "jenkins"
      operator: "Equal"
      value: "yes"
      effect: "NoExecute"
  volumes:
    - emptyDir: {}
      name: libcontainers
    - name: kaniko-secret
      secret:
        secretName: nexus-creds
"""

pipeline {
    agent {
        kubernetes {
            yaml podTemplate
        }
    }
    triggers {
        gitlab(triggerOnNoteRequest: true, noteRegex: "build")
    }
    options {
        timeout(time: 30, unit: 'MINUTES')
    }
    environment {
        VERSION = readMavenPom().getVersion()
        IMAGE_REPO = 'nexus.lppdev.pl:8084/financial-basic-data'
        GIT_CREDS = credentials('7e2b960b-acd3-4991-bcf0-302c340b736d')
    }
    stages {
        stage('Build') {
            steps {
                container("maven") {
                    sh 'mvn clean package'
                }
              }
        }

        stage('Docker Image') {
            parallel {
                stage('Develop') {
                    when {
                        branch 'develop'
                    }
                    steps {
                        container("kaniko") {
                             sh "/kaniko/executor --context $WORKSPACE --destination nexus.lppdev.pl:8084/financial-basic-data:${BRANCH_NAME} --digest-file $WORKSPACE/digest"
                        }
                    }
                }
                stage('Release/Hotfix') {
                    when {
                        anyOf {
                            branch 'hotfix/*'
                            branch 'release/*'
                        }
                    }
                    steps {
                        container("kaniko") {
                             sh "/kaniko/executor --context $WORKSPACE --destination nexus.lppdev.pl:8084/financial-basic-data:${VERSION} --digest-file $WORKSPACE/digest"
                        }
                    }
                }
                stage('Master') {
                    when {
                        anyOf {
                            branch 'master'
                        }
                    }
                    steps {
                        container("kaniko") {
                             sh "/kaniko/executor --context $WORKSPACE --destination nexus.lppdev.pl:8084/financial-basic-data:${VERSION} --digest-file $WORKSPACE/digest"
                        }
                    }
                }
            }
        }

        stage('Deploy to OKD') {
            parallel {
                stage('Arvato') {
                    when {
                        branch 'develop'
                    }
                    steps {
                        container('git') {
                            script {
                                def commit = sh(script: "git log -1 --pretty=%B", returnStdout: true).trim()
                                deployToTestEnvScript('arvato-test', 'develop', commit)
                            }
                        }
                    }
                }
                stage('Intuat') {
                    when {
                        anyOf {
                            branch 'hotfix/*'
                            branch 'release/*'
                        }
                    }
                    steps {
                        container('git') {
                            script {
                                def commit = sh(script: "git log -1 --pretty=%B", returnStdout: true).trim()
                                deployToTestEnvScript('integration-uat', VERSION, commit)
                            }
                        }
                    }
                }
            }
        }
    }
}

def deployToTestEnvScript(String envName, String version, String commit) {
    if (!(fileExists("financial-basic-data"))) {
        sh "git clone http://$GIT_CREDS_USR:$GIT_CREDS_PSW@gitlab.lppdev.pl/integration_systems/configuration/financial-basic-data.git"
        sh "git config --global user.email 'jenkins@lppsa.com'"
    }

    dir("financial-basic-data") {
        sh "cat $WORKSPACE/digest"
        sh "cd ./overlays/${envName} && kustomize edit set image $IMAGE_REPO@\$(cat $WORKSPACE/digest) && kustomize edit add annotation version:${version} --force"
        sh "ls -al && cat ./overlays/${envName}/kustomization.yaml"
        sh "git commit -am 'Publish to ${envName} new version from ${commit}' && git push origin master"
    }
}
