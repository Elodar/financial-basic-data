package com.jchdizd.financial.basic.data.mapper.currency;

import com.jchdizd.financial.basic.data.entity.BankSource;
import com.jchdizd.financial.basic.data.entity.CurrencyRate;
import com.jchdizd.financial.basic.data.entity.CurrencyType;
import com.jchdizd.financial.basic.data.entity.ExchangeRate;
import com.jchdizd.financial.basic.data.mapper.ExchangeRateKmdToCurrencyRateMapper;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

class ExchangeRateKmdToCurrencyRateMapperTest {
    private final static String CURRENCY = "USD";
    private final static String CURRENCY_NAME = "US Dollar";
    private final static LocalDate PUBLISH_DATE = LocalDate.of(2022, 10, 12);
    private final static BankSource SOURCE_ID = BankSource.NBP_A;
    private final static String ORIGINAL_CURRENCY = "PLN";
    private final ExchangeRateKmdToCurrencyRateMapper mapper = Mappers.getMapper(ExchangeRateKmdToCurrencyRateMapper.class);

    @Test
    void testMapper() {
        // given
        ExchangeRate exchangeRate = ExchangeRate.builder()
                .currency(CURRENCY)
                .currencyName(CURRENCY_NAME)
                .rate(BigDecimal.valueOf(509.0399998709482))
                .scale(100)
                .build();
        // when
        CurrencyRate currencyRate = mapper.map(exchangeRate, PUBLISH_DATE, SOURCE_ID, ORIGINAL_CURRENCY, exchangeRate.getScale());
        // then
        checkCurrencyRate(currencyRate);
    }

    private void checkCurrencyRate(CurrencyRate currencyRate) {
        assertThat(currencyRate).isNotNull();
        assertThat(currencyRate.getCurrencyCode()).isEqualTo("USD");
        assertThat(currencyRate.getEffectiveDate()).isEqualTo(PUBLISH_DATE);
        assertThat(currencyRate.getExchangeType()).isEqualTo(CurrencyType.Z);
        assertThat(currencyRate.getScale()).isEqualTo(BigDecimal.valueOf(100));
        assertThat(currencyRate.getExchangeRate()).isEqualTo(new BigDecimal("0.1964482163"));
        assertThat(currencyRate.getBankSource()).isEqualTo(SOURCE_ID);
        assertThat(currencyRate.getCurrencyCodeFrom()).isEqualTo(ORIGINAL_CURRENCY);
    }

    @Test
    void testReversedMapper() {
        // given
        ExchangeRate exchangeRate = ExchangeRate.builder()
                .currency(CURRENCY)
                .currencyName(CURRENCY_NAME)
                .rate(BigDecimal.valueOf(509.0399998709482))
                .scale(100)
                .build();
        // when
        CurrencyRate currencyRate = mapper.reverseMap(exchangeRate, PUBLISH_DATE, SOURCE_ID, ORIGINAL_CURRENCY, exchangeRate.getScale());
        // then
        checkReversedCurrencyRate(currencyRate);
    }

    private void checkReversedCurrencyRate(CurrencyRate currencyRate) {
        assertThat(currencyRate).isNotNull();
        assertThat(currencyRate.getCurrencyCode()).isEqualTo(ORIGINAL_CURRENCY);
        assertThat(currencyRate.getEffectiveDate()).isEqualTo(PUBLISH_DATE);
        assertThat(currencyRate.getExchangeType()).isEqualTo(CurrencyType.Z);
        assertThat(currencyRate.getScale()).isEqualTo(BigDecimal.valueOf(100));
        assertThat(currencyRate.getExchangeRate()).isEqualTo(new BigDecimal("5.0903999987"));
        assertThat(currencyRate.getBankSource()).isEqualTo(SOURCE_ID);
        assertThat(currencyRate.getCurrencyCodeFrom()).isEqualTo(CURRENCY);
    }
}
