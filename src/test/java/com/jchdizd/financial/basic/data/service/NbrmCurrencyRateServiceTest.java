package com.jchdizd.financial.basic.data.service;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.google.common.io.Resources;
import com.jchdizd.financial.basic.data.exception.CurrencyRateException;
import com.jchdizd.financial.basic.data.dto.nbrm.DsKurs;
import com.jchdizd.financial.basic.data.dto.nbrm.KursZbir;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
class NbrmCurrencyRateServiceTest {

    @Autowired
    private NbrmCurrencyRateService nbrmCurrencyRateService;
    private WireMockServer wireMockServer;

    @BeforeEach
    public void setup() throws IOException {
        wireMockServer = new WireMockServer(WireMockConfiguration.options()
                .port(8080)
                .usingFilesUnderClasspath("mappings"));
        wireMockServer.start();

        URL wsdl = Resources.getResource("wsdl/nbrm/nbrm.wsdl");
        String wsdlContent = Resources.toString(wsdl, StandardCharsets.UTF_8);
        URL xmlResponse = Resources.getResource("currency/response/currencyRatesNbrm.xml");
        String xmlResponseCont = Resources.toString(xmlResponse, StandardCharsets.UTF_8);

        WireMock.stubFor(WireMock.post(WireMock.urlEqualTo("/klservice/kurs.asmx"))
                .willReturn(WireMock.aResponse().withStatus(200).withBody(xmlResponseCont)));

        WireMock.stubFor(WireMock.get(WireMock.urlEqualTo("/klservice/kurs.asmx?wsdl"))
                .willReturn(WireMock.aResponse().withStatus(200).withBody(wsdlContent)));
    }

    @AfterEach
    public void teardown() {
        wireMockServer.stop();
    }

    @Test
    void shouldGetCurrencyRatesForSpecifiedDate() {
        //given
        LocalDate date = LocalDate.of(2023, 4, 18);
        //when
        DsKurs exchange = nbrmCurrencyRateService.getCurrencyValuesByDate(date);
        KursZbir firstKursZbir = exchange.getKursZbir().get(0);
        //then
        Assertions.assertNotNull(exchange);
        Assertions.assertEquals(71.0, firstKursZbir.getRBr());
        Assertions.assertEquals("2023-04-18", firstKursZbir.getDatum().toString());
        Assertions.assertEquals(978.0, firstKursZbir.getValuta());
        Assertions.assertEquals("EUR", firstKursZbir.getOznaka());
        Assertions.assertEquals("ЕМУ", firstKursZbir.getDrzava());
        Assertions.assertEquals(1.0, firstKursZbir.getNomin());
        Assertions.assertEquals(BigDecimal.valueOf(61.3255), firstKursZbir.getKupoven());
        Assertions.assertEquals(BigDecimal.valueOf(61.6337), firstKursZbir.getSreden());
        Assertions.assertEquals(BigDecimal.valueOf(61.9419), firstKursZbir.getProdazen());
        Assertions.assertEquals("EMU", firstKursZbir.getDrzavaAng());
        Assertions.assertEquals("UEM", firstKursZbir.getDrzavaAl());
        Assertions.assertEquals("евро", firstKursZbir.getNazivMak());
        Assertions.assertEquals("Euro", firstKursZbir.getNazivAng());
        Assertions.assertEquals("Euro", firstKursZbir.getValutaNazivAL());
        Assertions.assertEquals("2023-04-13", firstKursZbir.getDatumF().toString());
    }

    @Test
    void shouldThrowDifferentDatesException() {
        //given
        LocalDate date = LocalDate.now();
        //then
        assertThrows(CurrencyRateException.class, () -> {
            //when
            nbrmCurrencyRateService.getCurrencyValuesByDate(date);
        });
    }

}
