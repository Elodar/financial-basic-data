package com.jchdizd.financial.basic.data.repository;

import com.jchdizd.financial.basic.data.dto.CurrencyRequest;
import com.jchdizd.financial.basic.data.entity.BankSource;
import com.jchdizd.financial.basic.data.entity.CurrencyRate;
import com.jchdizd.financial.basic.data.entity.CurrencyType;
import com.jchdizd.financial.basic.data.provider.DataProvider;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class CurrencyRateRepositoryIntegrationTest {

    private static final String EUR = "EUR";
    private static final String PLN = "PLN";
    private static final String USD = "USD";
    private static final BigDecimal SCALE = BigDecimal.ONE;
    private static final BankSource NBP_A = BankSource.NBP_A;

    @Autowired
    private CurrencyRateRepository currencyRateRepository;

    @BeforeEach
    public void setUp() {
        currencyRateRepository.deleteAllInBatch();
        currencyRateRepository.saveAll(DataProvider.provideCurrencyRatesFromOneWeek());
        currencyRateRepository.save(CurrencyRate.builder()
                                            .currencyCode(EUR)
                                            .effectiveDate(LocalDate.of(2022, Month.AUGUST, 24))
                                            .exchangeType(CurrencyType.Z)
                                            .exchangeRate(new BigDecimal("0.2340166619"))
                                            .scale(SCALE)
                                            .currencyCodeFrom(PLN)
                                            .bankSource(BankSource.NBP_A)
                                            .build());
        currencyRateRepository.saveAllAndFlush(DataProvider.currencyRatesList());
    }

    @Test
    void findAllShouldProduceAllCurrencyRatesTest() {
        //given

        //when
        List<CurrencyRate> currencyRates = currencyRateRepository.findAll();

        //then
        assertThat(currencyRates).hasSize(15);
    }

    @Test
    void findAllByEffectiveDateBetweenAndExchangeTypeTest() {
        //given

        //when
        List<CurrencyRate> currencyRates = currencyRateRepository.findAllByEffectiveDateBetweenAndExchangeTypeAndBankSourceIn(
                LocalDate.of(2022, Month.AUGUST, 23),
                LocalDate.of(2022, Month.AUGUST, 26),
                CurrencyType.Z,
                List.of(BankSource.values())
        );

        //then
        assertThat(currencyRates).hasSize(9);
    }

    @Test
    void findAllByEffectiveDateBetweenAndCurrencyCodeAndCurrencyCodeFromAndBankSourceAndExchangeTypeTest() {
        //given

        //when
        List<CurrencyRate> currencyRates = currencyRateRepository.findAllByEffectiveDateBetweenAndCurrencyCodeAndCurrencyCodeFromAndBankSourceAndExchangeType(
                LocalDate.of(2022, Month.AUGUST, 23),
                LocalDate.of(2022, Month.AUGUST, 26),
                USD,
                PLN,
                NBP_A,
                CurrencyType.Z
        );

        //then
        assertThat(currencyRates).hasSize(4);
    }

    @Test
    void shouldFindCurrencyRate() {
        //given
        CurrencyRate currencyRate = DataProvider.currencyRatesList().get(0);

        //when
        Optional<CurrencyRate> currencyRateFromRepo =
                currencyRateRepository.findFirstByCurrencyCodeFromAndCurrencyCodeAndExchangeTypeAndBankSourceAndEffectiveDateIsLessThanEqualOrderByEffectiveDateDesc(
                        currencyRate.getCurrencyCodeFrom(),
                        currencyRate.getCurrencyCode(),
                        currencyRate.getExchangeType(),
                        currencyRate.getBankSource(),
                        currencyRate.getEffectiveDate()
                );

        //then
        assertTrue(currencyRateFromRepo.isPresent());
        assertThat(currencyRateFromRepo).contains(currencyRate);
    }

    @Test
    void shouldReturnCurrencyFrom05102022() {
        //given
        CurrencyRate currencyRate = DataProvider.currencyRatesList().get(1);

        //when
        Optional<CurrencyRate> currencyRateFromRepo =
                currencyRateRepository.findFirstByCurrencyCodeFromAndCurrencyCodeAndExchangeTypeAndBankSourceAndEffectiveDateIsLessThanEqualOrderByEffectiveDateDesc(
                        currencyRate.getCurrencyCodeFrom(),
                        currencyRate.getCurrencyCode(),
                        currencyRate.getExchangeType(),
                        currencyRate.getBankSource(),
                        LocalDate.parse("2022-10-07")
                );

        //then
        assertTrue(currencyRateFromRepo.isPresent());
        assertThat(currencyRateFromRepo.get().getEffectiveDate()).isEqualTo("2022-10-05");
        assertThat(currencyRateFromRepo.get().getExchangeRate()).isEqualByComparingTo(currencyRate.getExchangeRate());
        assertThat(currencyRateFromRepo).contains(currencyRate);
    }

    @Test
    void shouldFindAllBetweenStartToEndDateByCurrencyCodeAndCurrencyCodeFromAndBankSourceAndCurrencyType() {
        //given
        LocalDate startDate = LocalDate.parse("2022-10-07");
        LocalDate endDate = LocalDate.parse("2022-10-11");

        //when
        List<CurrencyRate> currencyRates = currencyRateRepository.findAllByEffectiveDateBetweenAndCurrencyCodeAndCurrencyCodeFromAndBankSourceAndExchangeType(
                startDate,
                endDate,
                USD,
                PLN,
                NBP_A,
                CurrencyType.Z
        );
        List<CurrencyRate> currencyRateBetweenStartAndEndDate = DataProvider.currencyRatesList().stream()
                                                                            .filter(currencyRate -> (
                                                                                    currencyRate.getEffectiveDate().isBefore(endDate) &&
                                                                                            currencyRate.getEffectiveDate().isAfter(startDate))
                                                                            ).toList();

        //then
        assertThat(currencyRates).hasSize(3).containsExactlyInAnyOrder(
                currencyRateBetweenStartAndEndDate.get(0),
                currencyRateBetweenStartAndEndDate.get(1),
                currencyRateBetweenStartAndEndDate.get(2)
        );
    }

    @Test
    void shouldReturnCurrencyRateWithCurrencyTypeY() {
        //given
        CurrencyRequest currencyDTO = CurrencyRequest.builder()
                .currencyFrom("PLN")
                .currencyTo("USD")
                .date(LocalDate.parse("2022-10-12"))
                .bankSource(BankSource.NBP_A)
                .currencyType(CurrencyType.Y)
                .build();
        //when
        Optional<CurrencyRate> currencyRateFromRepo =
                currencyRateRepository.findFirstByCurrencyCodeFromAndCurrencyCodeAndExchangeTypeAndBankSourceAndEffectiveDateIsLessThanEqualOrderByEffectiveDateDesc(
                        currencyDTO.getCurrencyFrom(),
                        currencyDTO.getCurrencyTo(),
                        currencyDTO.getCurrencyType(),
                        currencyDTO.getBankSource(),
                        currencyDTO.getDate()
                );

        //then
        assertNotNull(currencyRateFromRepo);
        assertTrue(currencyRateFromRepo.isPresent());
        assertThat(currencyDTO.getCurrencyFrom()).isEqualTo(currencyRateFromRepo.get().getCurrencyCodeFrom());
        assertThat(currencyDTO.getCurrencyTo()).isEqualTo(currencyRateFromRepo.get().getCurrencyCode());
        assertThat(currencyDTO.getDate()).isEqualTo(currencyRateFromRepo.get().getEffectiveDate());
        assertThat(currencyDTO.getCurrencyType()).isEqualTo(currencyRateFromRepo.get().getExchangeType());
    }
}
