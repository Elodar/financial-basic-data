package com.jchdizd.financial.basic.data.service;

import com.jchdizd.financial.basic.data.dto.byn.DailyExRates;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@DisplayName("Test for BynCurrencyFromBelarusService")
class BynCurrencyFromBelarusServiceTest {
    @Mock
    private BynFtpService bynFtpService;

    @InjectMocks
    private BynCurrencyFromBelarusService bynCurrencyFromBelarusService;

    private LocalDate date;
    private InputStream inputStream;

    @BeforeEach
    void setup() {
        date = LocalDate.of(2022, 1, 1);
        inputStream = getClass().getResourceAsStream("/2023_05_16.txt");
    }

    @Test
    @DisplayName("Should parse DailyExRates from FTP correctly for a given date")
    void downloadExRatesFromFtpForGivenDate() throws IOException, JAXBException {
        when(bynFtpService.searchFileInFTP(date)).thenReturn(inputStream);

        DailyExRates actual = bynCurrencyFromBelarusService.downloadExRatesFromFtp(date);

        // then
        assertNotNull(actual);
        assertEquals("036", actual.getCurrency().get(0).getNumCode());
        assertEquals("AUD", actual.getCurrency().get(0).getCharCode());
        assertEquals("1", actual.getCurrency().get(0).getScale());
        assertEquals("Аўстралійскі долар", actual.getCurrency().get(0).getName());
        assertEquals(BigDecimal.valueOf(2.0000), actual.getCurrency().get(0).getRate().setScale(1, RoundingMode.HALF_UP));
    }
}