package com.jchdizd.financial.basic.data.service;

import com.jchdizd.financial.basic.data.repository.CurrencyRateRepository;
import com.jchdizd.financial.basic.data.entity.BankSource;
import com.jchdizd.financial.basic.data.entity.CurrencyRate;
import com.jchdizd.financial.basic.data.entity.CurrencyType;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest
class CurrencyRateServiceUnitTest {
    @Mock
    private CurrencyRateRepository currencyRateRepository;
    @InjectMocks
    private CurrencyRateService currencyRateService;
    @Captor
    private ArgumentCaptor<CurrencyRate> captor;

    @Test
    void shouldCalculateWeeklyCurrencyRatesForAllCurrencies() {
        //given:
        LocalDate requestDate = LocalDate.now().with(TemporalAdjusters.previousOrSame(DayOfWeek.SATURDAY));
        LocalDate weekStartDate = requestDate.with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));

        doReturn(getCurrencyRates(requestDate)).when(currencyRateRepository).findAllByEffectiveDateBetweenAndExchangeTypeAndBankSourceIn(weekStartDate, requestDate, CurrencyType.Z, List.of(BankSource.values()));

        //when:
        currencyRateService.processWeeklyCurrencyRates(requestDate, Arrays.asList(BankSource.values()));

        //then:
        verify(currencyRateRepository, times(4)).save(captor.capture());
        List<CurrencyRate> resultList = captor.getAllValues().stream().sorted(Comparator.comparing(CurrencyRate::getExchangeRate)).collect(Collectors.toList());
        assertThat(resultList).hasSize(4);
        extracted(resultList.get(0), requestDate, "PLN", "USD", BigDecimal.valueOf(3), BankSource.NBP_A);
        extracted(resultList.get(1), requestDate, "PLN", "USD", BigDecimal.valueOf(6), BankSource.BNR);
        extracted(resultList.get(2), requestDate, "USD", "PLN", BigDecimal.valueOf(9), BankSource.BNR);
        extracted(resultList.get(3), requestDate, "JPY", "USD", BigDecimal.valueOf(25), BankSource.BNR);
    }

    private List<CurrencyRate> getCurrencyRates(LocalDate saturdayDate) {
        List<CurrencyRate> currencyRateList = new ArrayList<>();
        currencyRateList.add(currencyRate("USD", saturdayDate.minusDays(1), CurrencyType.Z, "PLN", BigDecimal.ONE, BankSource.BNR));
        currencyRateList.add(currencyRate("USD", saturdayDate.minusDays(2), CurrencyType.Z, "PLN", BigDecimal.valueOf(17), BankSource.BNR));
        currencyRateList.add(currencyRate("PLN", saturdayDate.minusDays(1), CurrencyType.Z, "USD", BigDecimal.ONE, BankSource.BNR));
        currencyRateList.add(currencyRate("PLN", saturdayDate.minusDays(2), CurrencyType.Z, "USD", BigDecimal.valueOf(11), BankSource.BNR));
        currencyRateList.add(currencyRate("JPY", saturdayDate.minusDays(1), CurrencyType.Z, "USD", BigDecimal.ONE, BankSource.BNR));
        currencyRateList.add(currencyRate("JPY", saturdayDate.minusDays(3), CurrencyType.O, "USD", BigDecimal.valueOf(49), BankSource.BNR));

        currencyRateList.add(currencyRate("PLN", saturdayDate.minusDays(1), CurrencyType.Z, "USD", BigDecimal.ONE, BankSource.NBP_A));
        currencyRateList.add(currencyRate("PLN", saturdayDate.minusDays(2), CurrencyType.Z, "USD", BigDecimal.valueOf(3), BankSource.NBP_A));
        currencyRateList.add(currencyRate("PLN", saturdayDate.minusDays(2), CurrencyType.Z, "USD", BigDecimal.valueOf(5), BankSource.NBP_A));

        return currencyRateList;
    }

    private void extracted(CurrencyRate currencyRate, LocalDate requestDate, String currencyCode, String currencyCodeFrom, BigDecimal exchangeRate, BankSource bankSource) {
        assertThat(currencyRate.getEffectiveDate()).isEqualTo(requestDate);
        assertThat(currencyRate.getCurrencyCode()).isEqualTo(currencyCode);
        assertThat(currencyRate.getCurrencyCodeFrom()).isEqualTo(currencyCodeFrom);
        assertThat(currencyRate.getExchangeType()).isEqualTo(CurrencyType.Y);
        assertThat(currencyRate.getBankSource()).isEqualTo(bankSource);
        assertThat(exchangeRate).isEqualByComparingTo(currencyRate.getExchangeRate());
    }

    private CurrencyRate currencyRate(String currencyCode, LocalDate effectiveDate, CurrencyType exchangeType, String currencyCodeFrom, BigDecimal exchangeRate, BankSource bankSource) {
        CurrencyRate currencyRate = new CurrencyRate();
        currencyRate.setCurrencyCode(currencyCode);
        currencyRate.setExchangeRate(exchangeRate);
        currencyRate.setEffectiveDate(effectiveDate);
        currencyRate.setExchangeType(exchangeType);
        currencyRate.setCurrencyCodeFrom(currencyCodeFrom);
        currencyRate.setBankSource(bankSource);
        return currencyRate;
    }
}
