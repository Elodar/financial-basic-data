package com.jchdizd.financial.basic.data.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jchdizd.financial.basic.data.controller.CurrencyRateController;
import com.jchdizd.financial.basic.data.controller.CurrencyRateScheduledController;
import com.jchdizd.financial.basic.data.dto.ProcessBankDateRequest;
import com.jchdizd.financial.basic.data.entity.BankSource;
import com.jchdizd.financial.basic.data.service.CurrencyRateService;
import com.jchdizd.financial.basic.data.service.DownloadCurrencyRateService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(controllers = CurrencyRateScheduledController.class)
public class DownloadCurrencyRateControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private CurrencyRateService currencyRateService;

    @MockBean
    private DownloadCurrencyRateService downloadCurrencyRateService;

    @Test
    void downloadAllBanks() throws Exception {
        mvc.perform(post(CurrencyRateController.URI_BASE + "/download-currency-rate")).andExpect(status().isOk());
    }

    @Test
    void downloadSelectedBank() throws Exception {
        ProcessBankDateRequest processBankDateRequest = new ProcessBankDateRequest();
        ObjectMapper objectMapper = new ObjectMapper();

        processBankDateRequest.setBankSource(BankSource.BNR);
        processBankDateRequest.setDate(LocalDate.now());
        mvc.perform(post(CurrencyRateController.URI_BASE + "/download-currency-rate-for-bank-and-day")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")
                .content(objectMapper.writeValueAsString(processBankDateRequest))
        ).andExpect(status().isOk());
    }
}
