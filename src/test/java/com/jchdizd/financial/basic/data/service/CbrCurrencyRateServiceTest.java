package com.jchdizd.financial.basic.data.service;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.google.common.io.Resources;
import com.jchdizd.financial.basic.data.exception.DifferentDatesException;
import com.jchdizd.financial.basic.data.config.cbr.CbrProperties;
import com.jchdizd.financial.basic.data.dto.cbr.ValCurs;
import com.jchdizd.financial.basic.data.dto.cbr.Valute;
import com.jchdizd.financial.basic.data.mapper.CbrCurrencyRatesMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CbrCurrencyRateServiceTest {

    private CbrCurrencyRateService cbrCurrencyRateService;
    private CbrCurrencyRatesMapper mapper;
    private CbrProperties cbrProperties;
    private WireMockServer wireMockServer;

    @BeforeEach
    void setUp() {
        cbrProperties = new CbrProperties();
        cbrProperties.setBase("http://localhost:8080");
        cbrProperties.setPath("/date?d=%s");
        cbrCurrencyRateService = new CbrCurrencyRateService(cbrProperties, new RestTemplate(), mapper);
        wireMockServer = new WireMockServer(8080);
        wireMockServer.start();
    }

    @AfterEach
    void tearDown() {
        wireMockServer.stop();
    }

    @Test
    void shouldGetCurrencyRatesForSpecifiedDate() throws IOException {
        //given
        LocalDate date = LocalDate.of(2023, 4, 18);
        stubForDate(date);
        ValCurs valCurs = cbrCurrencyRateService.getCurrencyValuesByDate(date);

        //then
        Valute firstValute = valCurs.getValutes().get(0);
        assertNotNull(valCurs);
        assertEquals(date, valCurs.getDate());
        assertEquals("Foreign Currency Market", valCurs.getName());
        assertEquals(43, valCurs.getValutes().size());
        assertEquals("R01010", firstValute.getId());
        assertEquals("036", firstValute.getNumCode());
        assertEquals("AUD", firstValute.getCharCode());
        assertEquals(1, firstValute.getNominal());
        assertEquals(BigDecimal.valueOf(54.8131), firstValute.getValue());
    }

    @Test
    void shouldThrowDifferentDatesException() throws IOException {
        //given
        LocalDate date = LocalDate.of(2023, 4, 20);
        stubForDate(date);
        //then
        assertThrows(DifferentDatesException.class, () -> {
            //when
            cbrCurrencyRateService.getCurrencyValuesByDate(date);
        });
    }

    private void stubForDate(LocalDate date) throws IOException {
        String formattedDate = date.format(DateTimeFormatter.ofPattern(CbrCurrencyRateService.DATE_FORMAT_FOR_CBR_PATH_VARIABLE));
        String path = String.format(cbrProperties.getPath(), formattedDate);

        URL xmlResponse = Resources.getResource("currency/response/currencyRatesCbr.xml");

        stubFor(WireMock.get(urlEqualTo(path))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/xml")
                        .withBody(Resources.toString(xmlResponse, StandardCharsets.UTF_8))));
    }
}
