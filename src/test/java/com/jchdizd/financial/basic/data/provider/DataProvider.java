package com.jchdizd.financial.basic.data.provider;

import com.jchdizd.financial.basic.data.entity.BankSource;
import com.jchdizd.financial.basic.data.entity.CurrenciesParams;
import com.jchdizd.financial.basic.data.entity.CurrencyRate;
import com.jchdizd.financial.basic.data.entity.CurrencyType;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.Month;
import java.util.List;

public class DataProvider {
    public static final String PLN = "PLN";
    public static final String USD = "USD";
    public static final BankSource NBP_A = BankSource.NBP_A;
    public static final BigDecimal SCALE_ONE = BigDecimal.ONE;

    public static List<CurrencyRate> provideCurrencyRatesFromOneMonth() {
        return List.of(
                createCurrencyRate(LocalDate.of(2022, Month.AUGUST, 30), new BigDecimal("0.2118195298"), CurrencyType.Z),
                createCurrencyRate(LocalDate.of(2022, Month.AUGUST, 29), new BigDecimal("0.2091131511"), CurrencyType.Z),
                createCurrencyRate(LocalDate.of(2022, Month.AUGUST, 26), new BigDecimal("0.2106815548"), CurrencyType.Z),
                createCurrencyRate(LocalDate.of(2022, Month.AUGUST, 25), new BigDecimal("0.2103226349"), CurrencyType.Z),
                createCurrencyRate(LocalDate.of(2022, Month.AUGUST, 24), new BigDecimal("0.2082075413"), CurrencyType.Z),
                createCurrencyRate(LocalDate.of(2022, Month.AUGUST, 23), new BigDecimal("0.2082032063"), CurrencyType.Z),
                createCurrencyRate(LocalDate.of(2022, Month.AUGUST, 19), new BigDecimal("0.2124901723"), CurrencyType.Z),
                createCurrencyRate(LocalDate.of(2022, Month.AUGUST, 18), new BigDecimal("0.2152018593"), CurrencyType.Z),
                createCurrencyRate(LocalDate.of(2022, Month.AUGUST, 17), new BigDecimal("0.2168303736"), CurrencyType.Z),
                createCurrencyRate(LocalDate.of(2022, Month.AUGUST, 16), new BigDecimal("0.2160667214"), CurrencyType.Z),
                createCurrencyRate(LocalDate.of(2022, Month.AUGUST, 12), new BigDecimal("0.2199784421"), CurrencyType.Z),
                createCurrencyRate(LocalDate.of(2022, Month.AUGUST, 11), new BigDecimal("0.221209574"), CurrencyType.Z),
                createCurrencyRate(LocalDate.of(2022, Month.AUGUST, 10), new BigDecimal("0.2168632894"), CurrencyType.Z),
                createCurrencyRate(LocalDate.of(2022, Month.AUGUST, 9), new BigDecimal("0.2177747773"), CurrencyType.Z),
                createCurrencyRate(LocalDate.of(2022, Month.AUGUST, 8), new BigDecimal("0.2167927678"), CurrencyType.Z),
                createCurrencyRate(LocalDate.of(2022, Month.AUGUST, 5), new BigDecimal("0.2172779419"), CurrencyType.Z),
                createCurrencyRate(LocalDate.of(2022, Month.AUGUST, 4), new BigDecimal("0.2157962883"), CurrencyType.Z),
                createCurrencyRate(LocalDate.of(2022, Month.AUGUST, 2), new BigDecimal("0.217466945"), CurrencyType.Z),
                createCurrencyRate(LocalDate.of(2022, Month.AUGUST, 1), new BigDecimal("0.21602938"), CurrencyType.Z),

                createReversedCurrencyRate(LocalDate.of(2022, Month.AUGUST, 30), new BigDecimal("4.7210"), CurrencyType.Z),
                createReversedCurrencyRate(LocalDate.of(2022, Month.AUGUST, 29), new BigDecimal("4.7821"), CurrencyType.Z),
                createReversedCurrencyRate(LocalDate.of(2022, Month.AUGUST, 26), new BigDecimal("4.7465"), CurrencyType.Z),
                createReversedCurrencyRate(LocalDate.of(2022, Month.AUGUST, 25), new BigDecimal("4.7546"), CurrencyType.Z),
                createReversedCurrencyRate(LocalDate.of(2022, Month.AUGUST, 24), new BigDecimal("4.8029"), CurrencyType.Z),
                createReversedCurrencyRate(LocalDate.of(2022, Month.AUGUST, 23), new BigDecimal("4.8030"), CurrencyType.Z),
                createReversedCurrencyRate(LocalDate.of(2022, Month.AUGUST, 19), new BigDecimal("4.7061"), CurrencyType.Z),
                createReversedCurrencyRate(LocalDate.of(2022, Month.AUGUST, 18), new BigDecimal("4.6468"), CurrencyType.Z),
                createReversedCurrencyRate(LocalDate.of(2022, Month.AUGUST, 17), new BigDecimal("4.6119"), CurrencyType.Z),
                createReversedCurrencyRate(LocalDate.of(2022, Month.AUGUST, 16), new BigDecimal("4.6282"), CurrencyType.Z),
                createReversedCurrencyRate(LocalDate.of(2022, Month.AUGUST, 12), new BigDecimal("4.5459"), CurrencyType.Z),
                createReversedCurrencyRate(LocalDate.of(2022, Month.AUGUST, 11), new BigDecimal("4.5206"), CurrencyType.Z),
                createReversedCurrencyRate(LocalDate.of(2022, Month.AUGUST, 10), new BigDecimal("4.6112"), CurrencyType.Z),
                createReversedCurrencyRate(LocalDate.of(2022, Month.AUGUST, 9), new BigDecimal("4.5919"), CurrencyType.Z),
                createReversedCurrencyRate(LocalDate.of(2022, Month.AUGUST, 8), new BigDecimal("4.6127"), CurrencyType.Z),
                createReversedCurrencyRate(LocalDate.of(2022, Month.AUGUST, 5), new BigDecimal("4.6024"), CurrencyType.Z),
                createReversedCurrencyRate(LocalDate.of(2022, Month.AUGUST, 4), new BigDecimal("4.6340"), CurrencyType.Z),
                createReversedCurrencyRate(LocalDate.of(2022, Month.AUGUST, 2), new BigDecimal("4.5984"), CurrencyType.Z),
                createReversedCurrencyRate(LocalDate.of(2022, Month.AUGUST, 1), new BigDecimal("4.6290"), CurrencyType.Z)
        );
    }

    public static List<CurrencyRate> provideCurrencyRatesFromOneWeek() {
        return List.of(
                createCurrencyRate(LocalDate.of(2022, Month.AUGUST, 26), new BigDecimal("0.2106815548"), CurrencyType.Z),
                createCurrencyRate(LocalDate.of(2022, Month.AUGUST, 25), new BigDecimal("0.2103226349"), CurrencyType.Z),
                createCurrencyRate(LocalDate.of(2022, Month.AUGUST, 24), new BigDecimal("0.2082075413"), CurrencyType.Z),
                createCurrencyRate(LocalDate.of(2022, Month.AUGUST, 23), new BigDecimal("0.2082032063"), CurrencyType.Z),
                createReversedCurrencyRate(LocalDate.of(2022, Month.AUGUST, 26), new BigDecimal("4.7465"), CurrencyType.Z),
                createReversedCurrencyRate(LocalDate.of(2022, Month.AUGUST, 25), new BigDecimal("4.7546"), CurrencyType.Z),
                createReversedCurrencyRate(LocalDate.of(2022, Month.AUGUST, 24), new BigDecimal("4.8029"), CurrencyType.Z),
                createReversedCurrencyRate(LocalDate.of(2022, Month.AUGUST, 23), new BigDecimal("4.8030"), CurrencyType.Z)
        );
    }

    public static List<CurrenciesParams> provideCurrenciesParams() {
        return List.of(
                CurrenciesParams.builder().currencyCodeFrom("PLN").currencyCodeTo("USD").bankSource(BankSource.NBP_A).build(),
                CurrenciesParams.builder().currencyCodeFrom("PLN").currencyCodeTo("EUR").bankSource(BankSource.NBP_A).build()
        );
    }

    public static List<CurrencyRate> currencyRatesList() {
        return List.of(
                createCurrencyRate(LocalDate.parse("2022-10-10"), BigDecimal.valueOf(5), CurrencyType.Z),
                createCurrencyRate(LocalDate.parse("2022-10-05"), BigDecimal.valueOf(4.5), CurrencyType.Z),
                createCurrencyRate(LocalDate.parse("2022-10-09"), BigDecimal.valueOf(4.9), CurrencyType.Z),
                createCurrencyRate(LocalDate.parse("2022-10-08"), BigDecimal.valueOf(4.8), CurrencyType.Z),
                createCurrencyRate(LocalDate.parse("2022-10-15"), BigDecimal.valueOf(4.8), CurrencyType.Z),
                createCurrencyRate(LocalDate.parse("2022-10-12"), BigDecimal.valueOf(4.8), CurrencyType.Y));
    }

    public static CurrencyRate createCurrencyRate(LocalDate currencyDate, BigDecimal rate, CurrencyType type) {
        CurrencyRate currencyRate = new CurrencyRate();
        currencyRate.setScale(SCALE_ONE);
        currencyRate.setExchangeType(type);
        currencyRate.setExchangeRate(rate);
        currencyRate.setEffectiveDate(currencyDate);
        currencyRate.setCurrencyCode(USD);
        currencyRate.setCurrencyCodeFrom(PLN);
        currencyRate.setBankSource(NBP_A);
        return currencyRate;
    }

    public static CurrencyRate createReversedCurrencyRate(LocalDate currencyDate, BigDecimal rate, CurrencyType type) {
        CurrencyRate currencyRate = new CurrencyRate();
        currencyRate.setScale(SCALE_ONE);
        currencyRate.setExchangeType(type);
        currencyRate.setExchangeRate(rate);
        currencyRate.setEffectiveDate(currencyDate);
        currencyRate.setCurrencyCode(PLN);
        currencyRate.setCurrencyCodeFrom(USD);
        currencyRate.setBankSource(NBP_A);
        return currencyRate;
    }

    public static CurrencyRate reverseCurrencyRate(CurrencyRate currencyRate) {
        BigDecimal reversedRate = BigDecimal.ONE.divide(currencyRate.getExchangeRate(), 6, RoundingMode.HALF_EVEN);
        reversedRate.divide(BigDecimal.ONE, 10, RoundingMode.HALF_EVEN);

        return CurrencyRate.builder()
                .currencyCode(currencyRate.getCurrencyCodeFrom())
                .currencyCodeFrom(currencyRate.getCurrencyCode())
                .exchangeRate(reversedRate)
                .bankSource(currencyRate.getBankSource())
                .effectiveDate(currencyRate.getEffectiveDate())
                .scale(currencyRate.getScale())
                .exchangeType(currencyRate.getExchangeType()).build();
    }
}
