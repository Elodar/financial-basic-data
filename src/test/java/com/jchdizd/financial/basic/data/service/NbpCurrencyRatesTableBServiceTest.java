package com.jchdizd.financial.basic.data.service;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.google.common.io.Resources;
import com.jchdizd.financial.basic.data.config.nbp.NbpProperties;
import com.jchdizd.financial.basic.data.dto.nbp.ExchangeRatesTable;
import com.jchdizd.financial.basic.data.mapper.NbpMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.jupiter.api.Assertions.*;

class NbpCurrencyRatesTableBServiceTest {
    private NbpCurrencyRatesTableBService currencyRatesTableBService;
    private NbpProperties nbpProperties;
    private final NbpMapper mapper = Mappers.getMapper(NbpMapper.class);

    private WireMockServer wireMockServer;

    @BeforeEach
    void init(){
        nbpProperties = new NbpProperties();
        nbpProperties.setBase("http://localhost:8080/exchangerates/tables/{table}/{date}");
        nbpProperties.setEnabled(true);
        nbpProperties.setPublicationHour(13);
        nbpProperties.setRangeOfDays(7);
        nbpProperties.setTableB("b");
        currencyRatesTableBService = new NbpCurrencyRatesTableBService(new RestTemplate(), mapper, nbpProperties);
        wireMockServer = new WireMockServer(8080);
        wireMockServer.start();
    }

    @AfterEach
    void clear() {
        wireMockServer.stop();
    }

    @Test
    void shouldGetCurrencyRatesForSpecifiedDate() throws IOException {
        //given
        LocalDate date = LocalDate.of(2023, 7, 19);
        setStub();
        ExchangeRatesTable exchangeRatesTable = currencyRatesTableBService.getCurrencyRatesFromTableNbp(date);

        //then
        assertNotNull(exchangeRatesTable);
        assertEquals(date, exchangeRatesTable.getEffectiveDate());
        assertEquals(BigDecimal.valueOf(0.046254), exchangeRatesTable.getRates().getRate().get(0).getMid());
        assertEquals("AFN", exchangeRatesTable.getRates().getRate().get(0).getCode());
        assertEquals("afgani (Afganistan)", exchangeRatesTable.getRates().getRate().get(0).getCurrency());
    }

    private void setStub() throws IOException {

        URL xmlResponse = Resources.getResource("currency/response/currencyRatesNbpB.xml");

        stubFor(WireMock.get(urlEqualTo("/exchangerates/tables/b/2023-07-19"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/xml")
                        .withBody(Resources.toString(xmlResponse, StandardCharsets.UTF_8))));
    }
}
