package com.jchdizd.financial.basic.data.service;

import com.jchdizd.financial.basic.data.dto.ecb.CubeList;
import com.jchdizd.financial.basic.data.dto.ecb.ResponeECB;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.xml.bind.JAXB;
import java.text.SimpleDateFormat;
import java.time.LocalDate;

@SpringBootTest
@RunWith(SpringRunner.class)
class EcbCurrencyRatesServiceTest {

    @Autowired
    private EcbCurrencyRatesService cubeService;

    @Test
    public void testGetCubesByDate() throws Exception {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        LocalDate date = LocalDate.parse("2023-04-17");

        CubeList cubeList = cubeService.getCubesByDate(date);
        ResponeECB expectedCubes = JAXB.unmarshal(getClass().getResourceAsStream("/currency/response/currencyRatesECB.xml"), ResponeECB.class);

        Assertions.assertEquals(expectedCubes.getCubeList().getCubesList().size(), cubeList.getCubesList().size());
        for (int i = 0; i < expectedCubes.getCubeList().getCubesList().size(); i++) {
            Assertions.assertEquals(expectedCubes.getCubeList().getCubesList().get(i).getCubeCurrencyRateList().get(i).getCurrencyName(), cubeList.getCubesList().get(i).getCubeCurrencyRateList().get(i).getCurrencyName());
            Assertions.assertEquals(expectedCubes.getCubeList().getCubesList().get(i).getCubeCurrencyRateList().get(i).getRateValue(), cubeList.getCubesList().get(i).getCubeCurrencyRateList().get(i).getRateValue());
        }

    }
}