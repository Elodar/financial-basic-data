package com.jchdizd.financial.basic.data.service;


import com.jchdizd.financial.basic.data.exception.CurrencyRateException;
import com.jchdizd.financial.basic.data.repository.CurrenciesParamsRepository;
import com.jchdizd.financial.basic.data.repository.CurrencyRateRepository;
import com.jchdizd.financial.basic.data.dto.CurrencyRequest;
import com.jchdizd.financial.basic.data.dto.CurrencyResponse;
import com.jchdizd.financial.basic.data.entity.BankSource;
import com.jchdizd.financial.basic.data.entity.CurrencyRate;
import com.jchdizd.financial.basic.data.entity.CurrencyType;
import com.jchdizd.financial.basic.data.mapper.ExchangeRateKmdToCurrencyRateMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
class CurrencyRateServiceTest {

    private final CurrencyRequest currencyRequest = CurrencyRequest.builder()
            .currencyFrom("PLN")
            .currencyType(CurrencyType.Z)
            .currencyTo("EUR")
            .date(LocalDate.parse("2022-10-15"))
            .bankSource(BankSource.NBP_A)
            .build();

    private final CurrencyRate currencyRate = new CurrencyRate("EUR", LocalDate.parse("2022-10-14"),
            CurrencyType.Z, BigDecimal.valueOf(5), BigDecimal.ONE, "PLN", BankSource.NBP_A);

    @Mock
    ExchangeRateKmdToCurrencyRateMapper mapper;
    @Mock
    private CurrencyRateRepository currencyRateRepository;
    @Mock
    private CurrenciesParamsRepository currenciesParamsRepository;
    @InjectMocks
    private CurrencyRateService currencyRateServiceTest;

    @BeforeEach
    public void setUp() {
        currencyRateServiceTest = new CurrencyRateService(currencyRateRepository, currenciesParamsRepository, mapper);
    }

    @Test
    void shouldCallCurrencyRateRepositoryFindFirstByCurrencyCodeFromAndCurrencyCodeAndExchangeTypeAndEffectiveDateIsLessThanEqualOrderByEffectiveDateDesc() {
        //given
        when(currencyRateRepository.findFirstByCurrencyCodeFromAndCurrencyCodeAndExchangeTypeAndBankSourceAndEffectiveDateIsLessThanEqualOrderByEffectiveDateDesc(anyString(),
                anyString(), any(),any(), any())).thenReturn(Optional.of(currencyRate));

        //when
        currencyRateServiceTest.getCurrencyRate(currencyRequest);

        //then
        verify(currencyRateRepository, times(1)).findFirstByCurrencyCodeFromAndCurrencyCodeAndExchangeTypeAndBankSourceAndEffectiveDateIsLessThanEqualOrderByEffectiveDateDesc(anyString(),
                anyString(), any(),any(), any());
    }


    @Test
    void shouldReturnCurrencyRate() {
        //given
        when(currencyRateRepository.findFirstByCurrencyCodeFromAndCurrencyCodeAndExchangeTypeAndBankSourceAndEffectiveDateIsLessThanEqualOrderByEffectiveDateDesc(anyString(),
                anyString(), any(),any(), any())).thenReturn(Optional.of(currencyRate));

        //when
        CurrencyResponse currencyResponse = currencyRateServiceTest.getCurrencyRate(currencyRequest);

        //then
        assertNotNull(currencyResponse);
        assertThat(currencyResponse.getExchangeRate()).isEqualTo(BigDecimal.valueOf(5));
        assertThat(currencyResponse.getCurrencyFrom()).isEqualTo(currencyRate.getCurrencyCodeFrom());
        assertThat(currencyResponse.getCurrencyTo()).isEqualTo(currencyRate.getCurrencyCode());
    }

    @Test
    void shouldThrowCurrencyRateException() {
        //given
        when(currencyRateRepository.findFirstByCurrencyCodeFromAndCurrencyCodeAndExchangeTypeAndBankSourceAndEffectiveDateIsLessThanEqualOrderByEffectiveDateDesc(
                anyString(),
                anyString(),
                any(),
                any(),
                any())
        ).thenReturn(Optional.empty());

        //when
        CurrencyRateException exception = Assertions.assertThrows(CurrencyRateException.class, () -> {
            currencyRateServiceTest.getCurrencyRate(currencyRequest);
        });

        //then
    }
}
