package com.jchdizd.financial.basic.data.mapper.currency;

import com.jchdizd.financial.basic.data.dto.bnr.BnrResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.xml.bind.JAXB;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;

class XmlToBnrResponseMapperTest {

    @Test
    void shouldConvertXmlToBnrResponse(){

        InputStream xmlResponse = getClass().getResourceAsStream("/currency/response/currencyRatesBnr.xml");
        BnrResponse bnrResponse = JAXB.unmarshal(xmlResponse, BnrResponse.class);

        Assertions.assertEquals("National Bank of Romania", bnrResponse.getHeader().getPublisher());
        Assertions.assertEquals(Date.valueOf("2023-04-25"), bnrResponse.getHeader().getPublishingDate());
        Assertions.assertEquals("DR", bnrResponse.getHeader().getMessageType());
        Assertions.assertEquals("Reference rates", bnrResponse.getBody().getSubject());
        Assertions.assertEquals("RON", bnrResponse.getBody().getOrigCurrency());
        Assertions.assertEquals(LocalDate.of(2023, 4, 18), bnrResponse.getBody().getCube().getDate());
        Assertions.assertEquals("HUF", bnrResponse.getBody().getCube().getRateList().get(12).getCurrency());
        Assertions.assertEquals(100, bnrResponse.getBody().getCube().getRateList().get(12).getMultiplier());
        Assertions.assertEquals(BigDecimal.valueOf(1.3303), bnrResponse.getBody().getCube().getRateList().get(12).getValue());
    }
}
