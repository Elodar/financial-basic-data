package com.jchdizd.financial.basic.data.rest;

import com.jchdizd.financial.basic.data.repository.CurrenciesParamsRepository;
import com.jchdizd.financial.basic.data.repository.CurrencyRateRepository;
import com.jchdizd.financial.basic.data.controller.CurrencyRateController;
import com.jchdizd.financial.basic.data.entity.BankSource;
import com.jchdizd.financial.basic.data.entity.CurrencyRate;
import com.jchdizd.financial.basic.data.entity.CurrencyType;
import com.jchdizd.financial.basic.data.provider.DataProvider;
import com.lppsa.integration.artifacts.accountancy.currency.ExchangeRate;
import com.lppsa.integration.artifacts.accountancy.currency.Source;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class CurrencyRateRestControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;
    @Autowired
    private CurrencyRateRepository currencyRateRepository;
    @Autowired
    private CurrenciesParamsRepository currenciesParamsRepository;

    @BeforeEach
    public void deleteAllInBatch() {
        currencyRateRepository.deleteAllInBatch();
    }

    @Test
    void shouldProcessNewCurrencyRate() {
        //given
        currencyRateRepository.saveAll(DataProvider.provideCurrencyRatesFromOneMonth());
        currenciesParamsRepository.saveAllAndFlush(DataProvider.provideCurrenciesParams());

        CurrencyRate expectedDailyCurrencyRate = DataProvider.createCurrencyRate(LocalDate.of(2022, Month.AUGUST, 31), new BigDecimal("0.2111486486"), CurrencyType.Z);
        CurrencyRate expectedDailyReversedCurrencyRate = DataProvider.createReversedCurrencyRate(LocalDate.of(2022, Month.AUGUST, 31), new BigDecimal("4.7360"), CurrencyType.Z);

        CurrencyRate expectedOperationCurrencyRateLast30Days = DataProvider.createCurrencyRate(LocalDate.of(2022, Month.AUGUST, 31), new BigDecimal("0.2143813379"), CurrencyType.O);
        CurrencyRate expectedOperationReversedCurrencyRateLast30Days = DataProvider.createReversedCurrencyRate(LocalDate.of(2022, Month.AUGUST, 31), new BigDecimal("4.6661157895"), CurrencyType.O);

        CurrencyRate expectedCorporationCurrencyRateLast30Days = DataProvider.createCurrencyRate(LocalDate.of(2022, Month.AUGUST, 31), new BigDecimal("0.2143813379"), CurrencyType.C);
        CurrencyRate expectedCorporationReversedCurrencyRateLast30Days = DataProvider.createReversedCurrencyRate(LocalDate.of(2022, Month.AUGUST, 31), new BigDecimal("4.6661157895"), CurrencyType.C);

        com.lppsa.integration.artifacts.accountancy.currency.CurrencyRate requestCurrencyRate = new com.lppsa.integration.artifacts.accountancy.currency.CurrencyRate();
        requestCurrencyRate.setPublishDate(LocalDate.of(2022, Month.AUGUST, 31));
        requestCurrencyRate.setSourceId(Source.NBP_A);
        requestCurrencyRate.setOriginalCurrency("PLN");

        ExchangeRate requestExchangeRate = new ExchangeRate();
        requestExchangeRate.setCurrency(DataProvider.USD);
        requestExchangeRate.setScale(1);
        requestExchangeRate.setRate(4.736);
        requestCurrencyRate.setExchangeRates(Collections.singletonList(requestExchangeRate));

        HttpHeaders httpHeaders = new HttpHeaders();
        HttpEntity<com.lppsa.integration.artifacts.accountancy.currency.CurrencyRate> httpEntity = new HttpEntity<>(requestCurrencyRate, httpHeaders);

        // when
        ResponseEntity<Void> response = restTemplate.postForEntity(CurrencyRateController.URI_BASE, httpEntity, Void.class);

        // then
        assertThat(response.getStatusCode().is2xxSuccessful()).isTrue();
        assertCurrencyRateWithDB(expectedDailyCurrencyRate);
        assertCurrencyRateWithDB(expectedDailyReversedCurrencyRate);
        assertCurrencyRateWithDB(expectedOperationCurrencyRateLast30Days);
        assertCurrencyRateWithDB(expectedOperationReversedCurrencyRateLast30Days);
        assertCurrencyRateWithDB(expectedCorporationCurrencyRateLast30Days);
        assertCurrencyRateWithDB(expectedCorporationReversedCurrencyRateLast30Days);
    }

    @Test
    void shouldSkipUnknownCurrencyParams() {
        //given
        currenciesParamsRepository.saveAllAndFlush(DataProvider.provideCurrenciesParams());

        String currencyCode = "EUR";
        com.lppsa.integration.artifacts.accountancy.currency.CurrencyRate requestCurrencyRate = new com.lppsa.integration.artifacts.accountancy.currency.CurrencyRate();
        requestCurrencyRate.setPublishDate(LocalDate.of(2020, Month.MARCH, 31));
        requestCurrencyRate.setSourceId(Source.NBP_B);
        requestCurrencyRate.setOriginalCurrency("PLN");
        ExchangeRate requestExchangeRate = new ExchangeRate();
        requestExchangeRate.setCurrency(currencyCode);
        requestExchangeRate.setScale(1);
        requestExchangeRate.setRate(0.2196691782);
        requestCurrencyRate.setExchangeRates(Collections.singletonList(requestExchangeRate));

        HttpHeaders httpHeaders = new HttpHeaders();
        HttpEntity<com.lppsa.integration.artifacts.accountancy.currency.CurrencyRate> httpEntity = new HttpEntity<>(requestCurrencyRate, httpHeaders);

        // when
        ResponseEntity<Void> response = restTemplate.postForEntity(CurrencyRateController.URI_BASE, httpEntity, Void.class);

        // then
        CurrencyRate notExpectedCurrency = CurrencyRate.builder()
                .currencyCode("EUR")
                .currencyCodeFrom("PLN")
                .exchangeType(CurrencyType.Z)
                .effectiveDate(LocalDate.of(2020, Month.MARCH, 31))
                .bankSource(BankSource.NBP_B)
                .build();
        assertThat(response.getStatusCode().is2xxSuccessful()).isTrue();
        assertFalse(currencyRateRepository.findAll().contains(notExpectedCurrency));
    }

    private void assertCurrencyRateWithDB(CurrencyRate expectedCurrencyRate) {
        CurrencyRate.Pk pk = new CurrencyRate.Pk(
                expectedCurrencyRate.getCurrencyCode(),
                expectedCurrencyRate.getEffectiveDate(),
                expectedCurrencyRate.getExchangeType(),
                expectedCurrencyRate.getCurrencyCodeFrom(),
                expectedCurrencyRate.getBankSource()
        );

        CurrencyRate createdWeeklyRate = currencyRateRepository.findById(pk).orElse(null);
        assertThat(createdWeeklyRate).isNotNull();
        assertThat(createdWeeklyRate.getCurrencyCode()).isEqualTo(expectedCurrencyRate.getCurrencyCode());
        assertThat(createdWeeklyRate.getCurrencyCodeFrom()).isEqualTo(expectedCurrencyRate.getCurrencyCodeFrom());
        assertThat(createdWeeklyRate.getEffectiveDate()).isEqualTo(expectedCurrencyRate.getEffectiveDate());
        assertThat(createdWeeklyRate.getScale()).isEqualTo(expectedCurrencyRate.getScale());
        assertThat(createdWeeklyRate.getBankSource()).isEqualTo(expectedCurrencyRate.getBankSource());
        assertThat(createdWeeklyRate.getExchangeType()).isEqualTo(expectedCurrencyRate.getExchangeType());
        assertThat(createdWeeklyRate.getExchangeRate()).isEqualByComparingTo(expectedCurrencyRate.getExchangeRate());
    }
}
