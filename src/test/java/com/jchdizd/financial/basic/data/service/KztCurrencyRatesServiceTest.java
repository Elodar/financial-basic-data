package com.jchdizd.financial.basic.data.service;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.google.common.io.Resources;
import com.jchdizd.financial.basic.data.config.kzt.KztProperties;
import com.jchdizd.financial.basic.data.dto.kzt.ExchangeRateDTO;
import com.jchdizd.financial.basic.data.mapper.KztCurrencyRatesMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class KztCurrencyRatesServiceTest {

    private KztCurrencyRatesService kztCurrencyRatesService;
    private KztProperties kztProperties;
    private final KztCurrencyRatesMapper mapper = Mappers.getMapper(KztCurrencyRatesMapper.class);
    private WireMockServer wireMockServer;

    @BeforeEach
    void init(){
        kztProperties = new KztProperties();
        kztProperties.setBase("http://localhost:8080/rss/get_rates.cfm?fdate={date}");
        kztProperties.setEnabled(true);
        kztProperties.setPublicationHour(15);
        kztProperties.setRangeOfDays(5);
        kztCurrencyRatesService = new KztCurrencyRatesService(kztProperties, new RestTemplate(), mapper);
        wireMockServer = new WireMockServer(8080);
        wireMockServer.start();
    }

    @AfterEach
    void tearDown() {
        wireMockServer.stop();
    }

    @Test
    void shouldGetCurrencyRatesForSpecifiedDate() throws IOException {
        //given
        LocalDate date = LocalDate.of(2023, 7, 20);
        setStub();
        ExchangeRateDTO rate = kztCurrencyRatesService.getCurrencyFromBank(date);

        //then
        assertNotNull(rate);
        assertEquals(date, rate.getDate());
        assertEquals(BigDecimal.valueOf(299.61), rate.getItems().get(0).getDescription());
        assertEquals("AUD", rate.getItems().get(0).getTitle());
        assertEquals("АВСТРАЛИЙСКИЙ ДОЛЛАР", rate.getItems().get(0).getFullname());
    }

    private void setStub() throws IOException {

        URL xmlResponse = Resources.getResource("currency/response/currencyRatesKzt.xml");

        stubFor(WireMock.get(urlEqualTo("/rss/get_rates.cfm?fdate=20.07.2023"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/xml")
                        .withBody(Resources.toString(xmlResponse, StandardCharsets.UTF_8))));
    }
}