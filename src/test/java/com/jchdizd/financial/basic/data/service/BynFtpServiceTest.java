package com.jchdizd.financial.basic.data.service;

import com.jchdizd.financial.basic.data.config.byn.BynProperties;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockftpserver.fake.FakeFtpServer;
import org.mockftpserver.fake.UserAccount;
import org.mockftpserver.fake.filesystem.DirectoryEntry;
import org.mockftpserver.fake.filesystem.FileEntry;
import org.mockftpserver.fake.filesystem.FileSystem;
import org.mockftpserver.fake.filesystem.UnixFakeFileSystem;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class BynFtpServiceTest {

    private FakeFtpServer fakeFtpServer;
    private BynProperties bynProperties;
    private int serverPort;
    private LocalDate testDate;

    @BeforeEach
    void setup() {
        fakeFtpServer = new FakeFtpServer();
        fakeFtpServer.addUserAccount(new UserAccount("username", "password", "/"));
        fakeFtpServer.setServerControlPort(0); // Port 0 oznacza przydzielenie losowego wolnego portu
        fakeFtpServer.start();
        serverPort = fakeFtpServer.getServerControlPort();
        testDate = LocalDate.of(2023, 5, 16);
        bynProperties = new BynProperties();
        bynProperties.setPath("/");
        bynProperties.setUser("username");
        bynProperties.setPassword("password");
        bynProperties.setServer("localhost");
        bynProperties.setPort(serverPort);
    }


    @Test
    void downloadFileFromFtpTest() throws Exception {
        //given
        fakeFtpServer.setServerControlPort(serverPort);

        URL resourceUrl = getClass().getResource("/2023_05_16.txt");
        Path resourcePath = Paths.get(resourceUrl.toURI());
        FileSystem fileSystem = new UnixFakeFileSystem();
        FileEntry fileEntry = new FileEntry("/2023_05_16.txt");
        byte[] fileContents = Files.readAllBytes(resourcePath);
        fileEntry.setContents(fileContents);

        fileSystem.add(new DirectoryEntry("/"));
        fileSystem.add(fileEntry);
        fakeFtpServer.setFileSystem(fileSystem);

        BynFtpService bynFtpService = new BynFtpService(bynProperties);
        //when

        InputStream fileFromFTP = bynFtpService.searchFileInFTP(testDate);
        //then

        assertNotNull(fileFromFTP);

        InputStream expectedFileContentInputStream = new ByteArrayInputStream(fileContents);
        String expectedFileContent = scanFileToString(expectedFileContentInputStream);
        String actualFileContent = scanFileToString(fileFromFTP);

        assertEquals(expectedFileContent, actualFileContent);
    }

    private static String scanFileToString(InputStream expectedFileContentInputStream) {
        Scanner scanner = new Scanner(expectedFileContentInputStream, StandardCharsets.UTF_8);
        String expectedFileContent = scanner.useDelimiter("\\A").next();
        scanner.close();
        return expectedFileContent;
    }
}
