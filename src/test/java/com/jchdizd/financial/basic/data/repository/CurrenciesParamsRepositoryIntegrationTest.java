package com.jchdizd.financial.basic.data.repository;

import com.jchdizd.financial.basic.data.entity.CurrenciesParams;
import com.jchdizd.financial.basic.data.provider.DataProvider;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class CurrenciesParamsRepositoryIntegrationTest {
    @Autowired
    private CurrenciesParamsRepository currenciesParamsRepository;

    @BeforeEach
    public void deleteAllInBatch() {
        currenciesParamsRepository.deleteAllInBatch();
    }

    @Test
    void findAllShouldProduceAllCurrenciesParamsTest() {
        //given
        currenciesParamsRepository.saveAllAndFlush(DataProvider.provideCurrenciesParams());
        //when
        List<CurrenciesParams> currenciesParams = currenciesParamsRepository.findAll();

        //then
        assertThat(currenciesParams).hasSize(2);
    }
}
