package com.jchdizd.financial.basic.data.service;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.google.common.io.Resources;
import com.jchdizd.financial.basic.data.entity.BankSource;
import com.jchdizd.financial.basic.data.repository.CurrenciesParamsRepository;
import com.jchdizd.financial.basic.data.repository.CurrencyRateRepository;
import com.jchdizd.financial.basic.data.entity.CurrenciesParams;
import com.jchdizd.financial.basic.data.entity.CurrencyRate;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.mockito.Mockito.doReturn;

@SpringBootTest
class DownloadCurrencyRateServiceTest {

    @Autowired
    private DownloadCurrencyRateService downloadCurrencyRateService;

    @Autowired
    private CurrencyRateRepository currencyRateRepository;

    @Autowired
    private CurrenciesParamsRepository currenciesParamsRepository;

    private WireMockServer wireMockServer;

    @BeforeEach
    void prepareData() throws IOException {
        CurrenciesParams currenciesParamsNbpA = createCurrenciesParams("EUR", "PLN", BankSource.NBP_A);
        CurrenciesParams currenciesParamsNbpB = createCurrenciesParams("AFN", "PLN", BankSource.NBP_B);
        CurrenciesParams currenciesParamsBNR = createCurrenciesParams("PLN", "RON", BankSource.BNR);
        CurrenciesParams currenciesParamsEcb = createCurrenciesParams("PLN", "EUR", BankSource.ECB);
        CurrenciesParams currenciesParamsDnb = createCurrenciesParams("PLN", "DKK", BankSource.DNB);
        CurrenciesParams currenciesParamsKzt = createCurrenciesParams("PLN", "KZT", BankSource.KZT);
        CurrenciesParams currenciesParamsCbr = createCurrenciesParams("BYN", "RUB", BankSource.CBR);
        CurrenciesParams currenciesParamsNbrm = createCurrenciesParams("EUR", "MKD", BankSource.NBRM);
        CurrenciesParams currenciesParamsSrb = createCurrenciesParams("EUR", "SEK", BankSource.SRB);

        currenciesParamsRepository.saveAll(List.of(currenciesParamsNbpA, currenciesParamsNbpB, currenciesParamsBNR, currenciesParamsEcb, currenciesParamsDnb, currenciesParamsKzt, currenciesParamsCbr, currenciesParamsNbrm, currenciesParamsSrb));
        currencyRateRepository.deleteAll();

        wireMockServer = new WireMockServer(8080);
        wireMockServer.start();

        URL xmlResponseNbpA = Resources.getResource("currency/response/currencyRatesNbpA.xml");

        URL xmlResponseNbpB = Resources.getResource("currency/response/currencyRatesNbpB.xml");

        URL xmlResponseBnr = Resources.getResource("currency/response/currencyRatesBnr.xml");

        URL xmlResponseEcb = Resources.getResource("currency/response/eu_stats_eurofxref_eurofxref-hist-90d.xml");

        URL xmlResponseDnb = Resources.getResource("currency/response/currencyRatesDnb.xml");

        URL xmlResponseKzt = Resources.getResource("currency/response/currencyRatesKzt.xml");

        URL xmlResponseCbr = Resources.getResource("currency/response/currencyRatesCbr.xml");

        URL nbrmWsdl = Resources.getResource("wsdl/nbrm/nbrm.wsdl");
        String nbrmWsdlContent = Resources.toString(nbrmWsdl, StandardCharsets.UTF_8);

        URL xmlResponseNbrm = Resources.getResource("currency/response/currencyRatesNbrm.xml");
        String xmlResponseContNbrm = Resources.toString(xmlResponseNbrm, StandardCharsets.UTF_8);

        URL srbWsdl = Resources.getResource("wsdl/srb/swea.wsdl");
        String srbWsdlContent = Resources.toString(srbWsdl, StandardCharsets.UTF_8);

        URL xmlResponseSrb = Resources.getResource("currency/response/currencyRatesSrb.xml");
        String xmlResponseContSrb = Resources.toString(xmlResponseSrb, StandardCharsets.UTF_8);

        stubFor(WireMock.get(urlEqualTo("/exchangerates/tables/a/2023-07-20"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/xml")
                        .withBody(Resources.toString(xmlResponseNbpA, StandardCharsets.UTF_8))));

        stubFor(WireMock.get(urlEqualTo("/exchangerates/tables/b/2023-07-19"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/xml")
                        .withBody(Resources.toString(xmlResponseNbpB, StandardCharsets.UTF_8))));

        stubFor(WireMock.get(urlEqualTo("/files/xml/curs_2023_4_18.xml"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/xml")
                        .withBody(Resources.toString(xmlResponseBnr, StandardCharsets.UTF_8))));

        WireMock.stubFor(WireMock.get(WireMock.urlEqualTo("/stats/eurofxref/eurofxref-hist-90d.xml"))
                .willReturn(WireMock.aResponse()
                        .withStatus(200)
                        .withBody(Resources.toString(xmlResponseEcb, StandardCharsets.UTF_8))));

        stubFor(WireMock.get(urlEqualTo("/api/currencyratesxmlhistory?lang=en"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/xml")
                        .withBody(Resources.toString(xmlResponseDnb, StandardCharsets.UTF_8))));

        stubFor(WireMock.get(urlEqualTo("/rss/get_rates.cfm?fdate=20.07.2023"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/xml")
                        .withBody(Resources.toString(xmlResponseKzt, StandardCharsets.UTF_8))));

        stubFor(WireMock.get(urlEqualTo("/scripts/XML_daily.asp?date_req=18/04/2023"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/xml")
                        .withBody(Resources.toString(xmlResponseCbr, StandardCharsets.UTF_8))));

        stubFor(WireMock.post(WireMock.urlEqualTo("/klservice/kurs.asmx"))
                .willReturn(WireMock.aResponse().withStatus(200).withBody(xmlResponseContNbrm)));

        stubFor(WireMock.get(WireMock.urlEqualTo("/klservice/kurs.asmx?wsdl"))
                .willReturn(WireMock.aResponse().withStatus(200).withBody(nbrmWsdlContent)));

        stubFor(WireMock.post(WireMock.urlEqualTo("/ws"))
                .willReturn(WireMock.aResponse().withStatus(200).withBody(xmlResponseContSrb)));

        stubFor(WireMock.get(WireMock.urlEqualTo("/sweaWS/wsdl/sweaWS.wsdl"))
                .willReturn(WireMock.aResponse().withStatus(200).withBody(srbWsdlContent)));

    }

    @AfterEach
    void cleanData() {
        wireMockServer.stop();
        currenciesParamsRepository.deleteAllInBatch();
        currencyRateRepository.deleteAllInBatch();
    }

    @Test
    void shouldDownloadAllTest() throws Exception {
        //given
        LocalDate date = LocalDate.of(2023, 7, 19);
        DownloadCurrencyRateService spy = Mockito.spy(downloadCurrencyRateService);

        //when
        doReturn(date).when(spy).getStartDay(Mockito.any());
        spy.downloadAllAvailableCurrencyRates();

        //then
        Mockito.verify(spy, Mockito.times(1)).processBankDate(BankSource.NBP_A, date);
        Mockito.verify(spy, Mockito.times(1)).processBankDate(BankSource.NBP_B, date);
        Mockito.verify(spy, Mockito.times(1)).processBankDate(BankSource.BNR, date);
        Mockito.verify(spy, Mockito.times(1)).processBankDate(BankSource.ECB, date);
        Mockito.verify(spy, Mockito.times(1)).processBankDate(BankSource.DNB, date);
        Mockito.verify(spy, Mockito.times(1)).processBankDate(BankSource.KZT, date);
        Mockito.verify(spy, Mockito.times(1)).processBankDate(BankSource.NBRM, date);
        Mockito.verify(spy, Mockito.times(1)).processBankDate(BankSource.CBR, date);
        Mockito.verify(spy, Mockito.times(1)).processBankDate(BankSource.BYN, date);
        Mockito.verify(spy, Mockito.times(1)).processBankDate(BankSource.SRB, date);
    }

    @Test
    void shouldDownloadNbpA() {
        //given

        //when
        downloadCurrencyRateService.processBankDate(BankSource.NBP_A, LocalDate.of(2023, 7, 20));

        //then
        long count = currencyRateRepository.count();
        List<CurrencyRate> currencyRateList = currencyRateRepository.findAll();
        Assertions.assertTrue(currencyRateList.stream()
                .anyMatch(currencyRate -> currencyRate.getCurrencyCode().equals("EUR"))
        );
        Assertions.assertEquals(7, count);
    }

    @Test
    void shouldDownloadNbpB() {
        //given

        //when
        downloadCurrencyRateService.processBankDate(BankSource.NBP_B, LocalDate.of(2023, 7, 19));

        //then
        long count = currencyRateRepository.count();
        List<CurrencyRate> currencyRateList = currencyRateRepository.findAll();
        Assertions.assertTrue(currencyRateList.stream()
                .anyMatch(currencyRate -> currencyRate.getCurrencyCode().equals("AFN"))
        );
        Assertions.assertEquals(7, count);
    }

    @Test
    void shouldDownloadBNR() {
        //given

        //when
        downloadCurrencyRateService.processBankDate(BankSource.BNR, LocalDate.of(2023, 4, 18));

        //then
        long count = currencyRateRepository.count();
        List<CurrencyRate> currencyRateList = currencyRateRepository.findAll();
        Assertions.assertTrue(currencyRateList.stream()
                .anyMatch(currencyRate -> currencyRate.getCurrencyCode().equals("RON"))
        );
        Assertions.assertEquals(7, count);
    }

    @Test
    void testDownloadECB() {
        //given

        //when
        downloadCurrencyRateService.processBankDate(BankSource.ECB, LocalDate.of(2023, 6, 5));

        //then
        long count = currencyRateRepository.count();
        List<CurrencyRate> currencyRateList = currencyRateRepository.findAll();
        Assertions.assertTrue(currencyRateList.stream()
                .anyMatch(currencyRate -> currencyRate.getCurrencyCode().equals("EUR"))
        );
        Assertions.assertEquals(7, count);
    }

    @Test
    void shouldDownloadDNB() {
        //given

        //when
        downloadCurrencyRateService.processBankDate(BankSource.DNB, LocalDate.of(2023, 7, 6));

        //then
        long count = currencyRateRepository.count();
        List<CurrencyRate> currencyRateList = currencyRateRepository.findAll();
        Assertions.assertTrue(currencyRateList.stream()
                .anyMatch(currencyRate -> currencyRate.getCurrencyCode().equals("DKK"))
        );
        Assertions.assertEquals(7, count);
    }

    @Test
    void shouldDownloadKzt() {
        //given

        //when
        downloadCurrencyRateService.processBankDate(BankSource.KZT, LocalDate.of(2023, 7, 20));

        //then
        long count = currencyRateRepository.count();
        List<CurrencyRate> currencyRateList = currencyRateRepository.findAll();
        Assertions.assertTrue(currencyRateList.stream()
                .anyMatch(currencyRate -> currencyRate.getCurrencyCode().equals("KZT"))
        );
        Assertions.assertEquals(7, count);
    }

    @Test
    void shouldDownloadCBR() {
        //given

        //when
        downloadCurrencyRateService.processBankDate(BankSource.CBR, LocalDate.of(2023, 4, 18));

        //then
        long count = currencyRateRepository.count();
        List<CurrencyRate> currencyRateList = currencyRateRepository.findAll();
        Assertions.assertTrue(currencyRateList.stream()
                .anyMatch(currencyRate -> currencyRate.getCurrencyCode().equals("RUB"))
        );
        Assertions.assertEquals(7, count);
    }

    @Test
    void shouldDownloadNBRM() {
        //given

        //when
        downloadCurrencyRateService.processBankDate(BankSource.NBRM, LocalDate.of(2023, 4, 18));

        //then
        long count = currencyRateRepository.count();
        List<CurrencyRate> currencyRateList = currencyRateRepository.findAll();
        Assertions.assertTrue(currencyRateList.stream()
                .anyMatch(currencyRate -> currencyRate.getCurrencyCode().equals("MKD"))
        );
        Assertions.assertEquals(7, count);
    }

    @Test
    void shouldDownloadSRB() {
        //given

        //when
        downloadCurrencyRateService.processBankDate(BankSource.SRB, LocalDate.of(2019, 5, 17));

        //then
        long count = currencyRateRepository.count();
        List<CurrencyRate> currencyRateList = currencyRateRepository.findAll();
        Assertions.assertTrue(currencyRateList.stream()
                .anyMatch(currencyRate -> currencyRate.getCurrencyCode().equals("EUR"))
        );
        Assertions.assertEquals(7, count);
    }

    private CurrenciesParams createCurrenciesParams(String currencyCodeTo, String currencyCodeFrom, BankSource bankSource) {
        CurrenciesParams currenciesParams = new CurrenciesParams();
        currenciesParams.setCurrencyCodeTo(currencyCodeTo);
        currenciesParams.setCurrencyCodeFrom(currencyCodeFrom);
        currenciesParams.setBankSource(bankSource);
        return currenciesParams;
    }
}

