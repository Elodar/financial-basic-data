package com.jchdizd.financial.basic.data.service;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.google.common.io.Resources;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import srb.Result;
import srb.ResultGroup;
import srb.ResultRow;
import srb.ResultSeries;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;

import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;

@SpringBootTest
public class SrbCurrencyRateServiceTest {

    @Autowired
    private SrbCurrencyRateService srbCurrencyRateService;
    private WireMockServer wireMockServer;

    @BeforeEach
    public void setup() throws IOException {
        wireMockServer = new WireMockServer(WireMockConfiguration.options()
                .port(8080)
                .usingFilesUnderClasspath("mappings"));
        wireMockServer.start();

        URL srbWsdl = Resources.getResource("wsdl/srb/swea.wsdl");
        String srbWsdlContent = Resources.toString(srbWsdl, StandardCharsets.UTF_8);

        URL xmlResponseSrb = Resources.getResource("currency/response/currencyRatesSrb.xml");
        String xmlResponseContSrb = Resources.toString(xmlResponseSrb, StandardCharsets.UTF_8);

        stubFor(WireMock.post(WireMock.urlEqualTo("/ws"))
                .willReturn(WireMock.aResponse().withStatus(200).withBody(xmlResponseContSrb)));

        stubFor(WireMock.get(WireMock.urlEqualTo("/sweaWS/wsdl/sweaWS.wsdl"))
                .willReturn(WireMock.aResponse().withStatus(200).withBody(srbWsdlContent)));
    }

    @AfterEach
    public void teardown() {
        wireMockServer.stop();
    }

    @Test
    void shouldGetCurrencyRatesForSpecifiedDate() {
        //given
        LocalDate date = LocalDate.of(2019, 5, 17);
        //when
        Result result = srbCurrencyRateService.getCurrencyRateByDateFromSrb(date);
        //then
        ResultGroup resultGroup = result.getGroups().get(0);
        ResultSeries resultSeries = resultGroup.getSeries().get(0);
        ResultRow resultRow = resultSeries.getResultrows().get(0);

        Assertions.assertNotNull(result);
        Assertions.assertNotNull(resultGroup);
        Assertions.assertNotNull(resultSeries);
        Assertions.assertNotNull(resultRow);
        Assertions.assertEquals("130", resultGroup.getGroupid().getValue());
        Assertions.assertEquals("Currencies against Swedish kronor", resultGroup.getGroupname().getValue());
        Assertions.assertEquals("SEKEURPMI", resultSeries.getSeriesid().getValue());
        Assertions.assertEquals("1 EUR", resultSeries.getSeriesname().getValue());
        Assertions.assertEquals("2019-05-17", resultRow.getDate().getValue().toString());
        Assertions.assertEquals("10.7763", resultRow.getValue().getValue().toString());

    }


}
