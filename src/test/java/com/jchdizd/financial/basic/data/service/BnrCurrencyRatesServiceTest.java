package com.jchdizd.financial.basic.data.service;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.google.common.io.Resources;
import com.jchdizd.financial.basic.data.config.bnr.BnrProperties;
import com.jchdizd.financial.basic.data.dto.bnr.Cube;
import com.jchdizd.financial.basic.data.mapper.BnrCurrencyRatesMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class BnrCurrencyRatesServiceTest {
    private static final String BNR_DATE_FORMAT = "yyyy_M_d";

    private BnrCurrencyRatesService service;
    private BnrCurrencyRatesMapper bnrCurrencyRatesMapper;
    private BnrProperties bnrProperties;
    private WireMockServer wireMockServer;

    @BeforeEach
    void setUp() {
        bnrProperties = new BnrProperties();
        bnrProperties.setBase("http://localhost:8080/files/xml/curs_{date}.xml");
        service = new BnrCurrencyRatesService(bnrProperties, bnrCurrencyRatesMapper);
        wireMockServer = new WireMockServer(8080);
        wireMockServer.start();
    }

    @AfterEach
    void tearDown() {
        wireMockServer.stop();
    }

    @Test
    void testGetBnrCurrencyRates() throws Exception {

        LocalDate date = LocalDate.of(2023, 4, 18);
        URL xmlResponse = Resources.getResource("currency/response/currencyRatesBnr.xml");

        // Given
        stubFor(WireMock.get(urlEqualTo("/files/xml/curs_2023_4_18.xml"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/xml")
                        .withBody(Resources.toString(xmlResponse, StandardCharsets.UTF_8))));

        // When
        Cube result = service.getBnrCurrencyRates(date);

        // Then
        assertNotNull(result);
    }
}