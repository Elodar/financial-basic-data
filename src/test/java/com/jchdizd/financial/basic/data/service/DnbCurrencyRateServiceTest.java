package com.jchdizd.financial.basic.data.service;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.google.common.io.Resources;
import com.jchdizd.financial.basic.data.exception.CurrencyRateException;
import com.jchdizd.financial.basic.data.config.dnb.DnbProperties;
import com.jchdizd.financial.basic.data.dto.dnb.Cube;
import com.jchdizd.financial.basic.data.mapper.DnbSingleCubeToCurrencyRateMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class DnbCurrencyRateServiceTest {

    private DnbCurrencyRateService dnbCurrencyRateService;
    private DnbProperties dnbProperties;
    private final DnbSingleCubeToCurrencyRateMapper mapper = Mappers.getMapper(DnbSingleCubeToCurrencyRateMapper.class);

    private WireMockServer wireMockServer;

    @BeforeEach
    void setUp() {
        dnbProperties = new DnbProperties();
        dnbProperties.setBase("http://localhost:8080");
        dnbProperties.setEnabled(true);
        dnbProperties.setPublicationHour(17);
        dnbProperties.setRangeOfDays(5);
        dnbCurrencyRateService = new DnbCurrencyRateService(dnbProperties, mapper, new RestTemplate());
        wireMockServer = new WireMockServer(8080);
        wireMockServer.start();
    }

    @AfterEach
    void tearDown() {
        wireMockServer.stop();
    }

    @Test
    void shouldGetCurrencyRatesForSpecifiedDate() throws IOException {
        //given
        LocalDate date = LocalDate.of(2023, 7, 12);
        stub();
        Cube cube = dnbCurrencyRateService.getCurrencyValuesByDate(date);

        //then
        assertNotNull(cube);
        assertEquals(date, cube.getTime());
        assertEquals(452.24, cube.currencies.get(0).getRate());
        assertEquals("AUD", cube.currencies.get(0).getCurrency());
        assertEquals("Australian dollars", cube.currencies.get(0).getName());
    }

    @Test
    void shouldThrowDifferentDatesException() throws IOException {
        //given
        LocalDate date = LocalDate.of(2023, 4, 20);
        stub();
        //then
        assertThrows(CurrencyRateException.class, () -> {
            //when
            dnbCurrencyRateService.getCurrencyValuesByDate(date);
        });
    }

    private void stub() throws IOException {

        URL xmlResponse = Resources.getResource("currency/response/currencyRatesDnb.xml");

        stubFor(WireMock.get(urlEqualTo("/"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/xml")
                        .withBody(Resources.toString(xmlResponse, StandardCharsets.UTF_8))));
    }
}
