create sequence if not exists CURRENCIES_PARAMS_SEQ
    minvalue 1
    start with 1
    increment by 1;