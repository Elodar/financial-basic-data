package com.jchdizd.financial.basic.data.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ExchangeRate implements Serializable {

    private static final long serialVersionUID = 1L;

    private String currency;

    private String currencyName;

    private BigDecimal rate;

    private Integer scale;
}
