package com.jchdizd.financial.basic.data.service;

import com.jchdizd.financial.basic.data.dto.nbp.ArrayOfExchangeRatesTable;
import com.jchdizd.financial.basic.data.dto.nbp.ExchangeRatesTable;
import com.jchdizd.financial.basic.data.entity.CurrencyRates;
import com.jchdizd.financial.basic.data.mapper.NbpMapper;
import com.jchdizd.financial.basic.data.utils.HttpUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public abstract class AbstractNbpCurrencyRatesService implements CurrencyRateServiceInterface {

    private final RestTemplate restTemplate;
    private final NbpMapper mapper;
    private final String url;

    @Override
    public CurrencyRates getCurrencyRateByDate(LocalDate date) {
        return map(getCurrencyRatesFromTableNbp(date));
    }

    public ExchangeRatesTable getCurrencyRatesFromTableNbp(LocalDate date) {
        ArrayOfExchangeRatesTable arrayOfExchangeRatesTable = HttpUtils.sendRequestForXMLAndParseToExpectedObject(getNbpUrlForTable(date), ArrayOfExchangeRatesTable.class, restTemplate);

        return arrayOfExchangeRatesTable.getExchangeRatesTable();
    }

    private CurrencyRates map(ExchangeRatesTable exchangeRatesTableDTO) {
        return CurrencyRates.builder()
                .publishDate(exchangeRatesTableDTO.getEffectiveDate())
                .originalCurrency("PLN")
                .exchangeRates(
                        exchangeRatesTableDTO.getRates().getRate().stream()
                                .map(mapper::map)
                                .toList()
                )
                .build();
    }

    private String getNbpUrlForTable(LocalDate date) {
        return url.replace("{date}", date.toString());
    }
}
