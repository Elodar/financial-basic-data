package com.jchdizd.financial.basic.data.mapper;

import com.jchdizd.financial.basic.data.dto.dnb.SingleCube;
import com.jchdizd.financial.basic.data.entity.ExchangeRate;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.math.BigDecimal;

@Mapper(componentModel = "spring")
public interface DnbSingleCubeToCurrencyRateMapper {

    @Mapping(target = "scale", constant = "100")
    @Mapping(target = "currencyName", source = "name")
    @Mapping(target = "rate", source = "rate", qualifiedByName = "mapRate")
    ExchangeRate map(SingleCube singleCube);

    @Named("mapRate")
    default BigDecimal mapRate(double rate) {
        return BigDecimal.valueOf(rate);
    }
}
