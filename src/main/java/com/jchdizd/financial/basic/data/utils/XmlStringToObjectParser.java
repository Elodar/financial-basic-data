package com.jchdizd.financial.basic.data.utils;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;

public class XmlStringToObjectParser {
    private XmlStringToObjectParser() {
        throw new IllegalStateException("Utility class");
    }

    public static <T> T parse(String xmlString, Class<T> clazz) throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(clazz);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        return clazz.cast(unmarshaller.unmarshal(new StringReader(xmlString)));
    }
}
