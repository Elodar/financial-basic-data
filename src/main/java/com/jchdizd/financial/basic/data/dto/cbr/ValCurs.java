package com.jchdizd.financial.basic.data.dto.cbr;

import com.jchdizd.financial.basic.data.adapter.DotSeperatedLocalDateAdapter;
import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@XmlRootElement(name = "ValCurs")
@XmlAccessorType(XmlAccessType.FIELD)
public class ValCurs {

    @XmlAttribute(name = "Date")
    @XmlJavaTypeAdapter(value = DotSeperatedLocalDateAdapter.class)
    private LocalDate date;

    @XmlAttribute(name = "name")
    private String name;

    @XmlElement(name = "Valute")
    private List<Valute> valutes = new ArrayList<>();
}