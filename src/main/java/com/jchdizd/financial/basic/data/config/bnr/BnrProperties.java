package com.jchdizd.financial.basic.data.config.bnr;

import com.jchdizd.financial.basic.data.config.BankProperties;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.validation.constraints.NotNull;

@Setter
@Getter
@Configuration
@ConfigurationProperties("currency.bnr")
public class BnrProperties implements BankProperties {

    @NotNull
    private String base;
    private boolean enabled;
    private int publicationHour;
    private long rangeOfDays;
}