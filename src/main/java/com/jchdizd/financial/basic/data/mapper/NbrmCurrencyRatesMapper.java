package com.jchdizd.financial.basic.data.mapper;

import com.jchdizd.financial.basic.data.dto.nbrm.KursZbir;
import com.jchdizd.financial.basic.data.entity.ExchangeRate;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface NbrmCurrencyRatesMapper {

    @Mapping(target = "scale", constant = "1")
    @Mapping(target = "currency", source = "oznaka")
    @Mapping(target = "currencyName", source = "nazivAng")
    @Mapping(target = "rate", source = "sreden")
    ExchangeRate map(KursZbir kursZbir);
}