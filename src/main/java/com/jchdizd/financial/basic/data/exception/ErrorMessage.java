package com.jchdizd.financial.basic.data.exception;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.time.LocalDateTime;

@Builder
@Getter
@ToString
public class ErrorMessage {
    private ErrorStatus status;
    private String message;
    private final LocalDateTime time = LocalDateTime.now();

    public enum ErrorStatus {
        ERROR, INTERNAL_ERROR
    }
}
