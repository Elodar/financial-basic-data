package com.jchdizd.financial.basic.data.dto.dnb;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
public class SingleCube {

    @XmlAttribute
    private double rate;
    @XmlAttribute
    private String currency;
    @XmlAttribute
    private String name;
}
