package com.jchdizd.financial.basic.data.dto.ecb;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import java.math.BigDecimal;

@Data
@XmlAccessorType(XmlAccessType.FIELD)
public class CubeCurrencyRate {

    @XmlAttribute(name = "currency")
    private String currencyName;

    @XmlAttribute(name = "rate")
    private BigDecimal rateValue;
}