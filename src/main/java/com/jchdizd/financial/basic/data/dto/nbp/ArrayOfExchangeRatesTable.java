package com.jchdizd.financial.basic.data.dto.nbp;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "ArrayOfExchangeRatesTable")
@XmlAccessorType(XmlAccessType.FIELD)
public class ArrayOfExchangeRatesTable {
    @XmlElement(name = "ExchangeRatesTable")
    private ExchangeRatesTable exchangeRatesTable;
}
