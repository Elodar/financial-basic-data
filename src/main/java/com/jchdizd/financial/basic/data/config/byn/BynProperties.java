package com.jchdizd.financial.basic.data.config.byn;

import com.jchdizd.financial.basic.data.config.BankProperties;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Setter
@Getter
@Configuration
@ConfigurationProperties("currency.byn")
public class BynProperties implements BankProperties {

    private String server;
    private String user;
    private String password;
    private String path;
    private int port;
    private boolean enabled;
    private int publicationHour;
    private long rangeOfDays;
}