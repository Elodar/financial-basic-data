package com.jchdizd.financial.basic.data.dto.kzt;

import com.jchdizd.financial.basic.data.adapter.DotSeperatedLocalDateAdapter;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDate;
import java.util.List;

@Data
@XmlRootElement(name = "rates")
@XmlAccessorType(XmlAccessType.FIELD)
public class ExchangeRateDTO {

    @XmlElement(name = "generator")
    private String generator;

    @XmlElement(name = "title")
    private String title;

    @XmlElement(name = "link")
    private String link;

    @XmlElement(name = "description")
    private String description;

    @XmlElement(name = "copyright")
    private String copyright;

    @XmlElement(name = "date")
    @XmlJavaTypeAdapter(value = DotSeperatedLocalDateAdapter.class)
    private LocalDate date;

    @XmlElement(name = "item")
    private List<RateItem> items;
}