package com.jchdizd.financial.basic.data.config.springfox.properties;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("swagger.advice.info")
@Setter
@Getter
@ToString
public class SwaggerProperties {
    private String title;
    private String description;
    private String contactName;
    private String contactMail;
}

