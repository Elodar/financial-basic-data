package com.jchdizd.financial.basic.data.adapter;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.datatype.XMLGregorianCalendar;
import java.time.LocalDate;

public class GregorianCalendarToLocalDateAdapter extends XmlAdapter<String, LocalDate> {

    @Override
    public LocalDate unmarshal(String xmlGregorianCalendarStr) throws Exception {
        XMLGregorianCalendar xmlGregorianCalendar = javax.xml.datatype.DatatypeFactory.newInstance()
                .newXMLGregorianCalendar(xmlGregorianCalendarStr);
        return xmlGregorianCalendar.toGregorianCalendar().toZonedDateTime().toLocalDate();
    }

    @Override
    public String marshal(LocalDate localDate) throws Exception {
        javax.xml.datatype.XMLGregorianCalendar xmlGregorianCalendar = javax.xml.datatype.DatatypeFactory.newInstance()
                .newXMLGregorianCalendar(localDate.getYear(), localDate.getMonthValue(), localDate.getDayOfMonth(), 0, 0, 0, 0, 0);
        return xmlGregorianCalendar.toXMLFormat();
    }
}
