package com.jchdizd.financial.basic.data;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties()
public class FinancialBasicDataApplication {
    public static void main(String[] args) {
        SpringApplication.run(FinancialBasicDataApplication.class, args);
    }
}
