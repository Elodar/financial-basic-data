package com.jchdizd.financial.basic.data.mapper;

import com.jchdizd.financial.basic.data.entity.ExchangeRate;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import srb.ResultRow;

@Mapper(componentModel = "spring")
public interface SrbExchangeRatesMapper {

    @Mapping(target = "scale", constant = "1")
    @Mapping(target = "currency", constant = "EUR")
    @Mapping(target = "currencyName", constant = "Euro")
    @Mapping(target = "rate", source = "value")
    ExchangeRate map(ResultRow resultRow);
}
