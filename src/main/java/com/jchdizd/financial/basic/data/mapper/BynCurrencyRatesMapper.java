package com.jchdizd.financial.basic.data.mapper;

import com.jchdizd.financial.basic.data.dto.byn.CurrencyXML;
import com.jchdizd.financial.basic.data.entity.ExchangeRate;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface BynCurrencyRatesMapper {

    @Mapping(target = "scale",source = "scale")
    @Mapping(target = "currencyName", source = "numCode")
    @Mapping(target = "currency", source = "charCode")
    @Mapping(target = "rate", source = "rate")
    ExchangeRate map(CurrencyXML currencyXML);
}
