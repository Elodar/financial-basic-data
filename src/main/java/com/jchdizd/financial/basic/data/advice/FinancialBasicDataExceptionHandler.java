package com.jchdizd.financial.basic.data.advice;

import com.jchdizd.financial.basic.data.exception.CurrencyRateException;
import com.jchdizd.financial.basic.data.exception.ErrorMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
@Slf4j
public class FinancialBasicDataExceptionHandler extends ResponseEntityExceptionHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = {CurrencyRateException.class})
    public ErrorMessage handleCurrencyNotFound(CurrencyRateException ex) {
        log.warn(ex.getMessage(), ex);

        return ErrorMessage.builder()
                .message(ex.getMessage())
                .status(ErrorMessage.ErrorStatus.ERROR)
                .build();
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value = {Exception.class})
    public ErrorMessage handleInternalServerError(Exception ex) {
        log.error(ex.getMessage(), ex);

        return ErrorMessage.builder()
                .message(ex.getMessage())
                .status(ErrorMessage.ErrorStatus.INTERNAL_ERROR)
                .build();
    }
}
