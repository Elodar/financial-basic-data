package com.jchdizd.financial.basic.data.service;

import com.jchdizd.financial.basic.data.config.bnr.BnrProperties;
import com.jchdizd.financial.basic.data.dto.bnr.BnrResponse;
import com.jchdizd.financial.basic.data.dto.bnr.Cube;
import com.jchdizd.financial.basic.data.entity.CurrencyRates;
import com.jchdizd.financial.basic.data.mapper.BnrCurrencyRatesMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXB;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Service
@RequiredArgsConstructor
public class BnrCurrencyRatesService implements CurrencyRateServiceInterface{

    private final BnrProperties bnrProperties;
    private final BnrCurrencyRatesMapper mapper;
    private static final String BNR_DATE_FORMAT = "yyyy_M_d";

    @Override
    public CurrencyRates getCurrencyRateByDate(LocalDate date) throws Exception {
        return map(getBnrCurrencyRates(date));
    }

    public Cube getBnrCurrencyRates(LocalDate date) throws MalformedURLException {

        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(BNR_DATE_FORMAT);
        String formattedDate = dateFormatter.format(date);

        String path = bnrProperties.getBase().replace("{date}", formattedDate);
        BnrResponse bnrResponse = JAXB.unmarshal(new URL(path), BnrResponse.class);

        return bnrResponse.getBody().getCube();
    }

    private CurrencyRates map(Cube cube) {
        return CurrencyRates.builder()
                .publishDate(cube.getDate())
                .originalCurrency("RON")
                .exchangeRates(
                        cube.getRateList().stream()
                                .map(mapper::map)
                                .toList()
                ).build();
    }
}