package com.jchdizd.financial.basic.data.service;

import com.jchdizd.financial.basic.data.config.srb.SrbProperties;
import com.jchdizd.financial.basic.data.entity.CurrencyRates;
import com.jchdizd.financial.basic.data.service.ws.WebServiceClientFactory;
import com.jchdizd.financial.basic.data.adapter.GregorianCalendarToLocalDateAdapter;
import com.jchdizd.financial.basic.data.mapper.SrbExchangeRatesMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import srb.AggregateMethodType;
import srb.LanguageType;
import srb.Result;
import srb.ResultRow;
import srb.SearchGroupSeries;
import srb.SearchRequestParameters;
import srb.ServiceException_Exception;
import srb.SweaWebServicePortType;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import javax.xml.ws.WebServiceException;
import java.time.LocalDate;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class SrbCurrencyRateService implements CurrencyRateServiceInterface {

    private final SrbProperties srbProperties;
    private final SrbExchangeRatesMapper srbMapper;
    private SweaWebServicePortType port;

    private void initializePortIfDoesntExist() {
        if (port == null) {
            QName serviceName = new QName(srbProperties.getService().getNamespace(), srbProperties.getService().getLocalPart());
            WebServiceClientFactory<SweaWebServicePortType> factory = new WebServiceClientFactory(srbProperties.getWsdlUrl(), serviceName, SweaWebServicePortType.class);
            port = factory.createClient();
        }
    }

    @Override
    public CurrencyRates getCurrencyRateByDate(LocalDate date) {
        return map(getCurrencyRateByDateFromSrb(date));
    }


    public srb.Result getCurrencyRateByDateFromSrb(LocalDate date) {

        GregorianCalendarToLocalDateAdapter gregorianCalendarToLocalDateAdapter = new GregorianCalendarToLocalDateAdapter();

        XMLGregorianCalendar xmlGregorianCalendarDate;
        try {
            xmlGregorianCalendarDate = DatatypeFactory.newInstance()
                    .newXMLGregorianCalendar(gregorianCalendarToLocalDateAdapter.marshal(date));

        } catch (java.lang.Exception e) {
            throw new RuntimeException("Cannot parse provided localDate to gregorianCalendar for daily SRB currency processing.", e);
        }


        try {
            initializePortIfDoesntExist();
            SearchGroupSeries searchGroupSeries = new SearchGroupSeries();
            searchGroupSeries.setGroupid("130");
            searchGroupSeries.setSeriesid("SEKEURPMI");
            SearchRequestParameters searchRequestParameters = new SearchRequestParameters();
            searchRequestParameters.setAggregateMethod(AggregateMethodType.D);
            searchRequestParameters.setLanguageid(LanguageType.EN);
            searchRequestParameters.setDatefrom(xmlGregorianCalendarDate);
            searchRequestParameters.setDateto(xmlGregorianCalendarDate);
            searchRequestParameters.getSearchGroupSeries().add(searchGroupSeries);

            return port.getInterestAndExchangeRates(searchRequestParameters);
        } catch (ServiceException_Exception e) {
            throw new WebServiceException("Failed to get exchange rates from SRB bank", e);
        }
    }

    private CurrencyRates map(Result result) {
        List<ResultRow> resultRows = result.getGroups().get(0).getSeries().get(0).getResultrows();
        try {
            GregorianCalendarToLocalDateAdapter gregorianCalendarToLocalDateAdapter = new GregorianCalendarToLocalDateAdapter();
            return CurrencyRates.builder()
                    .publishDate(gregorianCalendarToLocalDateAdapter.unmarshal(resultRows.get(0).getDate().getValue().toXMLFormat()))
                    .originalCurrency("SEK")
                    .exchangeRates(resultRows.stream()
                            .map(srbMapper::map)
                            .toList()
                    ).build();
        } catch (Exception e) {
            throw new RuntimeException("Failed to map SRB ResultRow to CurrencyRate", e);
        }
    }
}
