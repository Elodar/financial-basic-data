package com.jchdizd.financial.basic.data.dto.nbp;

import lombok.Data;

import javax.xml.bind.annotation.*;
import java.util.List;

@Data
@XmlRootElement(name = "Rates")
@XmlAccessorType(XmlAccessType.FIELD)
public class Rates {

    @XmlElement(name = "Rate")
    private List<Rate> rate;
}