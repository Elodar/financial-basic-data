package com.jchdizd.financial.basic.data.service;

import com.jchdizd.financial.basic.data.exception.CurrencyRateException;
import com.jchdizd.financial.basic.data.config.dnb.DnbProperties;
import com.jchdizd.financial.basic.data.dto.dnb.Cube;
import com.jchdizd.financial.basic.data.dto.dnb.Envelope;
import com.jchdizd.financial.basic.data.entity.CurrencyRates;
import com.jchdizd.financial.basic.data.mapper.DnbSingleCubeToCurrencyRateMapper;
import com.jchdizd.financial.basic.data.utils.HttpUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class DnbCurrencyRateService implements CurrencyRateServiceInterface {

    private final DnbProperties dnbProperties;
    private final DnbSingleCubeToCurrencyRateMapper mapper;
    private final RestTemplate restTemplate;

    @Override
    public CurrencyRates getCurrencyRateByDate(LocalDate date) {
        return map(getCurrencyValuesByDate(date));
    }

    public Cube getCurrencyValuesByDate(LocalDate date) {
        Envelope envelope = HttpUtils.sendRequestForXMLAndParseToExpectedObject(dnbProperties.getBase(), Envelope.class, restTemplate);
        return findSingeCubeByDate(date, envelope)
                .orElseThrow(() -> new CurrencyRateException(String.format("No exchange rates from bank DNB were found for the day: %s", date)));
    }

    private Optional<Cube> findSingeCubeByDate(LocalDate date, Envelope envelope) {
        return envelope.getCubeData().getCubes().stream()
                .filter(cube -> date.equals(cube.getTime()))
                .findFirst();
    }

    private CurrencyRates map(Cube cube) {
        return CurrencyRates.builder()
                .publishDate(cube.getTime())
                .originalCurrency("DKK")
                .exchangeRates(
                        cube.getCurrencies().stream()
                                .map(cubeCurrencyRate -> mapper.map(cubeCurrencyRate))
                                .toList()
                ).build();
    }

}
