package com.jchdizd.financial.basic.data.dto.ecb;

import com.lppsa.integration.artifacts.accountancy.adapter.LocalDateAdapter;
import lombok.Data;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDate;
import java.util.List;

@Data
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(namespace = "http://www.ecb.int/vocabulary/2002-08-01/eurofxref")
public class Cube {

    @XmlAttribute(name = "time")
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    private LocalDate dateOfCurrencyRates;

    @XmlElement(name = "Cube", namespace = "http://www.ecb.int/vocabulary/2002-08-01/eurofxref")
    private List<CubeCurrencyRate> cubeCurrencyRateList;
}