package com.jchdizd.financial.basic.data.exception;

public class HttpCodeException extends RuntimeException {

    public static final String HTTP_CODE_EXCEPTION_MESSAGE = "HTTP error code: %s with message: %s";

    public HttpCodeException(String message) {
        super(message);
    }
}
