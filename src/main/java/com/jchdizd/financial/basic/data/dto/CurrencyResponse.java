package com.jchdizd.financial.basic.data.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;

@Getter
@Setter
@SuperBuilder
public class CurrencyResponse extends Currency {

    private BigDecimal exchangeRate;
}
