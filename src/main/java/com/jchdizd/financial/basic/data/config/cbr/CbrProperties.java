package com.jchdizd.financial.basic.data.config.cbr;

import com.jchdizd.financial.basic.data.config.BankProperties;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.validation.constraints.NotNull;

@Setter
@Getter
@Configuration
@ConfigurationProperties("currency.cbr")
public class CbrProperties implements BankProperties {

    @NotNull
    private String base;
    @NotNull
    private String path;
    private boolean enabled;
    private int publicationHour;
    private long rangeOfDays;
}
