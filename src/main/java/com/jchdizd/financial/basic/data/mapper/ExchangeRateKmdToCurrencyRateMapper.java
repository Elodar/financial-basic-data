package com.jchdizd.financial.basic.data.mapper;

import com.jchdizd.financial.basic.data.entity.BankSource;
import com.jchdizd.financial.basic.data.entity.CurrencyRate;
import com.jchdizd.financial.basic.data.entity.ExchangeRate;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;

@Mapper(componentModel = "spring")
public interface ExchangeRateKmdToCurrencyRateMapper {

    @Mapping(target = "currencyCode", source = "exchangeRate.currency")
    @Mapping(target = "effectiveDate", source = "publishDate")
    @Mapping(target = "exchangeType", constant = "Z")
    @Mapping(target = "exchangeRate", source = "exchangeRate.rate", qualifiedByName = "divideRate")
    @Mapping(target = "scale", source = "exchangeRate.scale")
    @Mapping(target = "currencyCodeFrom", source = "originalCurrency")
    @Mapping(target = "bankSource", source = "sourceId")
    CurrencyRate map(ExchangeRate exchangeRate, LocalDate publishDate, BankSource sourceId, String originalCurrency, @Context Integer scale);

    @Named("divideRate")
    default BigDecimal divideRate(Double in, @Context Integer scale) {
        return BigDecimal.valueOf(scale).divide(BigDecimal.valueOf(in), 10, RoundingMode.HALF_EVEN);
    }

    @Mapping(target = "currencyCode", source = "originalCurrency")
    @Mapping(target = "effectiveDate", source = "publishDate")
    @Mapping(target = "exchangeType", constant = "Z")
    @Mapping(target = "exchangeRate", source = "exchangeRate.rate", qualifiedByName = "scaleRate")
    @Mapping(target = "scale", source = "exchangeRate.scale")
    @Mapping(target = "currencyCodeFrom", source = "exchangeRate.currency")
    @Mapping(target = "bankSource", source = "sourceId")
    CurrencyRate reverseMap(ExchangeRate exchangeRate, LocalDate publishDate, BankSource sourceId, String originalCurrency, @Context Integer scale);

    @Named("scaleRate")
    default BigDecimal scaleRate(Double in, @Context Integer scale) {
        return BigDecimal.valueOf(in).divide(BigDecimal.valueOf(scale), 10, RoundingMode.HALF_EVEN);
    }

    @Named("bankSource")
    default String bankSource(BankSource bankSource) {
        return bankSource.name();
    }
}

