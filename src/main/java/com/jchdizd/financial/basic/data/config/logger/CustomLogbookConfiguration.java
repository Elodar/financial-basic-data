package com.jchdizd.financial.basic.data.config.logger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.zalando.logbook.DefaultSink;
import org.zalando.logbook.Logbook;
import org.zalando.logbook.json.JsonHttpLogFormatter;

import static org.zalando.logbook.Conditions.contentType;
import static org.zalando.logbook.Conditions.exclude;
import static org.zalando.logbook.Conditions.requestTo;


@Configuration
public class CustomLogbookConfiguration {

    @Bean
    public Logbook logbook() {
        return Logbook.builder()
                .condition(exclude(
                        requestTo("/financial-basic-data/actuator/**"),
                        requestTo("/financial-basic-data/admin/**"),
                        contentType("application/octet-stream")
                ))
                .sink(new DefaultSink(
                        new CustomJsonHttpLogFormatter(new JsonHttpLogFormatter()),
                        new InfoLevelHttpLogWriter()
                ))
                .build();
    }
}

