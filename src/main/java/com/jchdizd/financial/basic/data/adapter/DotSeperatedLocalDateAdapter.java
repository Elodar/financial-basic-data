package com.jchdizd.financial.basic.data.adapter;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DotSeperatedLocalDateAdapter extends XmlAdapter<String, LocalDate> {

    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    @Override
    public LocalDate unmarshal(String dateString) {
        return LocalDate.parse(dateString, DATE_FORMATTER);
    }

    @Override
    public String marshal(LocalDate date) {
        return DATE_FORMATTER.format(date);
    }
}
