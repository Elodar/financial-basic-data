package com.jchdizd.financial.basic.data.exception;

public class DifferentDatesException extends RuntimeException {

    public static final String DIFFERENT_DATES_EXCEPTION_MESSAGE = "The bank %s returned currency rates with date: %s, but expected for date: %s";

    public DifferentDatesException(String message) {
        super(message);
    }
}
