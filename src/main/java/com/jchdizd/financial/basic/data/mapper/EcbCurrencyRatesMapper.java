package com.jchdizd.financial.basic.data.mapper;

import com.jchdizd.financial.basic.data.dto.ecb.CubeCurrencyRate;
import com.jchdizd.financial.basic.data.entity.ExchangeRate;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Mapper(componentModel = "spring")
public interface EcbCurrencyRatesMapper {

    @Mapping(target = "scale", constant = "1")
    @Mapping(target = "currencyName", source = "currencyName")
    @Mapping(target = "currency", source = "currencyName")
    @Mapping(target = "rate", source = "rateValue", qualifiedByName = "divideRate")
    ExchangeRate map(CubeCurrencyRate cubeCurrencyRate);

    @Named("divideRate")
    default BigDecimal divideRate(Double in) {
        return BigDecimal.ONE.divide(BigDecimal.valueOf(in), 10, RoundingMode.HALF_EVEN);
    }
}
