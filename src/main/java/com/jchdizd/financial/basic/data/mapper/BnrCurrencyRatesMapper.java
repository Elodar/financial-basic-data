package com.jchdizd.financial.basic.data.mapper;

import com.jchdizd.financial.basic.data.dto.bnr.Rate;
import com.jchdizd.financial.basic.data.entity.ExchangeRate;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

@Mapper(componentModel = "spring")
public interface BnrCurrencyRatesMapper {

    @Named("multiplayerMapper")
    default Integer mapMultiplier(Integer multiplier) {
        return multiplier != null && multiplier != 0 ? multiplier : 1;
    }

    @Mapping(target = "scale", source = "multiplier", qualifiedByName = "multiplayerMapper")
    @Mapping(target = "currencyName", source = "currency")
    @Mapping(target = "currency", source = "currency")
    @Mapping(target = "rate", source = "value")
    ExchangeRate map(Rate rate);
}
