package com.jchdizd.financial.basic.data.adapter;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.math.BigDecimal;

public class CommaToDotAdapter extends XmlAdapter<String, BigDecimal> {

    @Override
    public BigDecimal unmarshal(String value) throws Exception {
        return new BigDecimal(value.replace(',', '.'));
    }

    @Override
    public String marshal(BigDecimal value) throws Exception {
        return value.toString();
    }
}
