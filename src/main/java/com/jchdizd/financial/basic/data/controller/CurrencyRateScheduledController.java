package com.jchdizd.financial.basic.data.controller;

import com.jchdizd.financial.basic.data.dto.CurrencyRateWeeklyRequest;
import com.jchdizd.financial.basic.data.dto.ProcessBankDateRequest;
import com.jchdizd.financial.basic.data.service.DownloadCurrencyRateService;
import com.jchdizd.financial.basic.data.service.CurrencyRateService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping(CurrencyRateController.URI_BASE)
@RequiredArgsConstructor
@Slf4j
public class CurrencyRateScheduledController {

    static final String URI_WEEKLY_NBP_RATES = "/calculate-weekly-rates";
    static final String URI_DOWNLOAD_CURRENCY_RATE = "/download-currency-rate";

    static final String URI_DOWNLOAD_CURRENCY_RATE_FOR_BANK = "/download-currency-rate-for-bank-and-day";

    private final CurrencyRateService currencyRateService;

    private final DownloadCurrencyRateService downloadCurrencyRateService;

    @PostMapping(value = URI_WEEKLY_NBP_RATES)
    public void calculateWeeklyNbpRates(@Valid @RequestBody CurrencyRateWeeklyRequest requestDTO) {
        currencyRateService.processWeeklyCurrencyRates(requestDTO.getRequestDate(), requestDTO.getBankSource());
    }

    @PostMapping(value = URI_DOWNLOAD_CURRENCY_RATE)
    public void downloadCurrencyRate() {
        downloadCurrencyRateService.downloadAllAvailableCurrencyRates();
    }

    @PostMapping(value = URI_DOWNLOAD_CURRENCY_RATE_FOR_BANK)
    public void processBankDate(@RequestBody @NotNull ProcessBankDateRequest processBankDateRequest) {
        downloadCurrencyRateService.processBankDate(processBankDateRequest.getBankSource(), processBankDateRequest.getDate());
    }

}
