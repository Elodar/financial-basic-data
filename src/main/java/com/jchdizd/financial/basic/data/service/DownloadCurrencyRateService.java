package com.jchdizd.financial.basic.data.service;

import com.jchdizd.financial.basic.data.entity.BankSource;
import com.jchdizd.financial.basic.data.config.BankProperties;
import com.jchdizd.financial.basic.data.entity.CurrencyRates;
import com.jchdizd.financial.basic.data.entity.DownloadCurrencyRateStatus;
import com.jchdizd.financial.basic.data.entity.DownloadStatus;
import com.jchdizd.financial.basic.data.repository.DownloadCurrencyRateStatusRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Slf4j
public class DownloadCurrencyRateService {

    private BankService bankService;
    private CurrencyRateProperties currencyRateProperties;
    private CurrencyRateService currencyRateService;
    private DownloadCurrencyRateStatusRepository downloadCurrencyRateStatusRepository;

    public void downloadAllAvailableCurrencyRates() {

        this.getAllBanks().forEach(bank -> {
            try {
                BankProperties bankProperties = currencyRateProperties.getProperties(bank);
                if (bankProperties.isEnabled()) {
                    this.processBank(bank, bankProperties.getRangeOfDays());
                } else {
                    log.info(String.format("Bank %s is disabled", bank.name()));
                }
            } catch (Exception e) {
                log.error(String.format("Error during processing bank %s: %s", bank.name(), e.getMessage()));
            }
        });

    }

    private Set<BankSource> getAllBanks() {
        return Arrays.stream(BankSource.values()).collect(Collectors.toSet());
    }

    private void processBank(BankSource bank, long rangeOfDays) throws Exception {
        for (LocalDate date : getListOfDays(bank, rangeOfDays)) {
            if (isWorkingDay(date) && !isValidDownloaded(bank, date)) {
                processBankDate(bank, date);
            }
        }
    }

    private void saveCurrencyRateForDate(CurrencyRates currencyRate) {
        currencyRateService.processCurrencyRates(currencyRate);
    }

    private void saveStatus(DownloadStatus status, BankSource bank, LocalDate effectiveDate) {
        Optional<DownloadCurrencyRateStatus> downloadCurrentStatus = downloadCurrencyRateStatusRepository.findById(new DownloadCurrencyRateStatus.Pk(effectiveDate, bank));
        DownloadCurrencyRateStatus downloadCurrencyRateStatus = downloadCurrentStatus.orElseGet(() -> DownloadCurrencyRateStatus.builder().bankSource(bank).effectiveDate(effectiveDate).build());
        downloadCurrencyRateStatus.setStatus(status);
        downloadCurrencyRateStatusRepository.save(downloadCurrencyRateStatus);
    }

    private CurrencyRates getCurrencyRateForDate(BankSource bankSource, LocalDate date) throws Exception {
        CurrencyRates currencyRates = bankService.getBankService(bankSource).getCurrencyRateByDate(date);
        currencyRates.setSourceId(bankSource);
        return currencyRates;
    }

    private boolean isValidDownloaded(BankSource bankSource, LocalDate date) {
        DownloadCurrencyRateStatus downloadCurrencyRateStatus = downloadCurrencyRateStatusRepository.getDownloadCurrencyRateStatusByBankSourceAndEffectiveDate(bankSource, date);
        return downloadCurrencyRateStatus != null && downloadCurrencyRateStatus.getStatus().equals(DownloadStatus.OK);
    }

    private boolean isWorkingDay(LocalDate date) {
        return List.of(DayOfWeek.MONDAY,
                DayOfWeek.TUESDAY,
                DayOfWeek.WEDNESDAY,
                DayOfWeek.THURSDAY,
                DayOfWeek.FRIDAY
        ).contains(date.getDayOfWeek());
    }

    private List<LocalDate> getListOfDays(BankSource bank, long rangeOfDays) throws Exception {
        List<LocalDate> result = new ArrayList<>();
        LocalDate localDate = this.getStartDay(bank);
        for (int i = 0; i < rangeOfDays; i++) {
            result.add(localDate);
            localDate = localDate.minusDays(1);
        }
        return result;
    }

    public LocalDate getStartDay(BankSource bank) throws Exception {
        BankProperties bankProperties = currencyRateProperties.getProperties(bank);
        return bankProperties.getPublicationHour() < LocalDateTime.now().getHour()
                ? LocalDate.now()
                : LocalDate.now().minusDays(1);
    }

    public void processBankDate(BankSource bankSource, LocalDate date) {
        try {
            CurrencyRates currencyRates = getCurrencyRateForDate(bankSource, date);
            saveCurrencyRateForDate(currencyRates);
            saveStatus(DownloadStatus.OK, bankSource, date);
        } catch (Exception e) {
            log.error(e.getMessage());
            saveStatus(DownloadStatus.ERROR, bankSource, date);
        }
    }
}
