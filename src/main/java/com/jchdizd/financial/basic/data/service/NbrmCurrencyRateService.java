package com.jchdizd.financial.basic.data.service;

import com.jchdizd.financial.basic.data.exception.CurrencyRateException;
import com.jchdizd.financial.basic.data.service.ws.WebServiceClientFactory;
import com.jchdizd.financial.basic.data.config.nbrm.NbrmProperties;
import com.jchdizd.financial.basic.data.dto.nbrm.DsKurs;
import com.jchdizd.financial.basic.data.dto.nbrm.KursZbir;
import com.jchdizd.financial.basic.data.entity.CurrencyRates;
import com.jchdizd.financial.basic.data.mapper.NbrmCurrencyRatesMapper;
import com.jchdizd.financial.basic.data.utils.XmlStringToObjectParser;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nbrm.KursSoap;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class NbrmCurrencyRateService implements CurrencyRateServiceInterface {

    private final NbrmProperties nbrmProperties;
    private final NbrmCurrencyRatesMapper mapper;
    private KursSoap port;
    public static final String DATE_FORMAT_FOR_NBRM_PATH_VARIABLE = "dd.MM.yyyy";

    @Override
    public CurrencyRates getCurrencyRateByDate(LocalDate date) throws Exception {
        return map(getCurrencyValuesByDate(date));
    }

    public DsKurs getCurrencyValuesByDate(LocalDate dateToFind) {
        DsKurs dsKurs = sendRequestForDsKurs(dateToFind);
        List<KursZbir> kursZbirsWithMatchingDates = List.of();
        if (dsKurs != null && dsKurs.getKursZbir() != null) {
            kursZbirsWithMatchingDates = dsKurs.getKursZbir().stream()
                    .filter(kursZbir -> {
                        LocalDate datum = kursZbir.getDatum();
                        return datum.equals(dateToFind);
                    })
                    .toList();
        }
        if (kursZbirsWithMatchingDates.isEmpty()) {
            throw new CurrencyRateException(String.format("No exchange rates from bank NBRM were found for the day: %s", dateToFind));
        }

        dsKurs.setKursZbir(kursZbirsWithMatchingDates); //NOSONAR
        log.info("Returning {} DsKurs for date {}", dsKurs.getKursZbir(), dateToFind);
        return dsKurs;
    }

    private DsKurs sendRequestForDsKurs(LocalDate dateToFind) {
        initializePortIfDoesntExist();
        String formattedDate = dateToFind.format(DateTimeFormatter.ofPattern(DATE_FORMAT_FOR_NBRM_PATH_VARIABLE));
        String response = port.getExchangeRates(formattedDate, formattedDate);
        try {
            return XmlStringToObjectParser.parse(response, DsKurs.class);
        } catch (JAXBException e) {
            log.error(e.getMessage());
        }
        return null;
    }

    private void initializePortIfDoesntExist() {
        if (port == null) {
            QName serviceName = new QName(nbrmProperties.getService().getNamespace(), nbrmProperties.getService().getLocalPart());
            WebServiceClientFactory<KursSoap> factory = new WebServiceClientFactory(nbrmProperties.getWsdlUrl(), serviceName, KursSoap.class);
            port = factory.createClient();
        }
    }

    private CurrencyRates map(DsKurs dsKurs) {
        return CurrencyRates.builder()
                .publishDate(dsKurs.getKursZbir().get(0).getDatum())
                .originalCurrency("MKD")
                .exchangeRates(
                        dsKurs.getKursZbir().stream()
                                .map(mapper::map)
                                .toList()
                ).build();
    }
}
