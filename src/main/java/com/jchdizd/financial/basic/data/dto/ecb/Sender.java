package com.jchdizd.financial.basic.data.dto.ecb;

import javax.xml.bind.annotation.XmlElement;

public class Sender {
    @XmlElement(name = "name", namespace = "http://www.gesmes.org/xml/2002-08-01")
    private String name;
}