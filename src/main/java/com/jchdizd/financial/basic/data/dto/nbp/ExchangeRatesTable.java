package com.jchdizd.financial.basic.data.dto.nbp;

import com.lppsa.integration.artifacts.accountancy.adapter.LocalDateAdapter;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDate;

@Data
@XmlRootElement(name = "ExchangeRatesTable")
@XmlAccessorType(XmlAccessType.FIELD)
public class ExchangeRatesTable {

    @XmlElement(name = "Table")
    private String table;

    @XmlElement(name = "No")
    private String no;

    @XmlElement(name = "EffectiveDate")
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    private LocalDate effectiveDate;

    @XmlElement(name = "Rates")
    private Rates rates;
}