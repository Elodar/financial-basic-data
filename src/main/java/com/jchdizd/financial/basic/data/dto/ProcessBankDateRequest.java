package com.jchdizd.financial.basic.data.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.jchdizd.financial.basic.data.entity.BankSource;
import lombok.Data;

import java.time.LocalDate;

@Data
public class ProcessBankDateRequest {

    private BankSource bankSource;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate date;
}
