package com.jchdizd.financial.basic.data.entity;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Builder
@Entity
@Table(name = "CURRENCIES_PARAMS", schema = "CURRENCY")
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CurrenciesParams {

    @Id
    @SequenceGenerator(name = "CURRENCIES_PARAMS_SEQ", sequenceName = "CURRENCIES_PARAMS_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CURRENCIES_PARAMS_SEQ")
    @Column(name = "ID")
    private Integer id;

    @Column(name = "CURRENCY_CODE_FROM")
    private String currencyCodeFrom;

    @Column(name = "CURRENCY_CODE_TO")
    private String currencyCodeTo;

    @Column(name = "BANK_SOURCE")
    @Enumerated(EnumType.STRING)
    private BankSource bankSource;
}
