package com.jchdizd.financial.basic.data.validator.constraint;

import com.jchdizd.financial.basic.data.validator.DateOnlySaturdayValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = DateOnlySaturdayValidator.class)
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface DateOnlySaturday {
    String message() default "{validation.message.date.only.saturday}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
