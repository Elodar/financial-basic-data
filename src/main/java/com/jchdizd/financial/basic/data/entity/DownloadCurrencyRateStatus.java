package com.jchdizd.financial.basic.data.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "DOWNLOAD_STATUS", schema = "CURRENCY")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@IdClass(DownloadCurrencyRateStatus.Pk.class)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class DownloadCurrencyRateStatus {


    @Id
    @Column(name = "EFFECTIVE_DATE", nullable = false)
    @EqualsAndHashCode.Include
    private LocalDate effectiveDate;

    @Id
    @Column(name = "BANK_SOURCE")
    @EqualsAndHashCode.Include
    @Enumerated(EnumType.STRING)
    private BankSource bankSource;

    @Column(name = "STATUS")
    @EqualsAndHashCode.Include
    @Enumerated(EnumType.STRING)
    private DownloadStatus status;

    @Column(name = "COUNT_OF_ATTENTION")
    private Integer countOfAttention;

    @Column(name = "ERROR")
    private String error;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Pk implements Serializable {
        private LocalDate effectiveDate;
        private BankSource bankSource;
    }


}
