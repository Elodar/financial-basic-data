package com.jchdizd.financial.basic.data.entity;

public enum BankSource {
    NBP_A,
    NBP_B,
    BNR,
    ECB,
    DNB,
    SRB,
    KZT,
    BYN,
    CBR,
    NBRM
}
