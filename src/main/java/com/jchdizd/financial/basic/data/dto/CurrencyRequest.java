package com.jchdizd.financial.basic.data.dto;

import com.jchdizd.financial.basic.data.entity.CurrencyType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;

@Getter
@Setter
@ToString
@SuperBuilder
public class CurrencyRequest extends Currency {

    public static CurrencyRequest createCurrencyDTO(String from, String to, LocalDate date, CurrencyType currencyType) {
        return CurrencyRequest.builder()
                .currencyFrom(from)
                .currencyTo(to)
                .date(date)
                .currencyType(currencyType).build();
    }
}
