package com.jchdizd.financial.basic.data.repository;

import com.jchdizd.financial.basic.data.entity.CurrenciesParams;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CurrenciesParamsRepository extends JpaRepository<CurrenciesParams, Integer> {
}
