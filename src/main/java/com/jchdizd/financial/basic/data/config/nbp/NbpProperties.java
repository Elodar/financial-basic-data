package com.jchdizd.financial.basic.data.config.nbp;

import com.jchdizd.financial.basic.data.config.BankProperties;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@Configuration
@ConfigurationProperties("currency.nbp")
public class NbpProperties implements BankProperties {

    @NotNull
    private String base;
    private String tableA;
    private String tableB;
    private boolean enabled;
    private int publicationHour;
    private long rangeOfDays;

    public String getNbpUrlForTableA(){
        return base.replace("{table}", tableA);
    }

    public String getNbpUrlForTableB(){
        return base.replace("{table}", tableB);
    }
}
