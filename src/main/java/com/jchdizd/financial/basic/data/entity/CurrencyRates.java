package com.jchdizd.financial.basic.data.entity;


import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.List;

@Data
@Builder
public class CurrencyRates implements Serializable {

    private static final long serialVersionUID = 1L;


    private BankSource sourceId;

    private LocalDate publishDate;

    private String originalCurrency;

    private String additionalInformation;

    private List<ExchangeRate> exchangeRates;

    private OffsetDateTime createdAt;
}
