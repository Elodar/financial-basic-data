package com.jchdizd.financial.basic.data.validator;

import com.jchdizd.financial.basic.data.validator.constraint.DateOnlySaturday;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.DayOfWeek;
import java.time.LocalDate;

public class DateOnlySaturdayValidator implements ConstraintValidator<DateOnlySaturday, LocalDate> {
    @Override
    public boolean isValid(LocalDate date, ConstraintValidatorContext constraintValidatorContext) {
        return date != null && (date.getDayOfWeek() == DayOfWeek.SATURDAY);
    }
}
