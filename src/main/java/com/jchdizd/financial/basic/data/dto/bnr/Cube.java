package com.jchdizd.financial.basic.data.dto.bnr;

import com.lppsa.integration.artifacts.accountancy.adapter.LocalDateAdapter;
import lombok.Data;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDate;
import java.util.List;

@Data
@XmlRootElement(name = "Cube")
@XmlAccessorType(XmlAccessType.FIELD)
public class Cube {

    @XmlAttribute(name = "date")
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    private LocalDate date;

    @XmlElement(name = "Rate", namespace = "http://www.bnr.ro/xsd")
    private List<Rate> rateList;
}