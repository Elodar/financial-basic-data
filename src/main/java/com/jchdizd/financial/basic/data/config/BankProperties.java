package com.jchdizd.financial.basic.data.config;


public interface BankProperties {


    int getPublicationHour();

    boolean isEnabled();

    long getRangeOfDays();
}
