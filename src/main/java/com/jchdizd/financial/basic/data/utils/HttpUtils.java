package com.jchdizd.financial.basic.data.utils;

import com.jchdizd.financial.basic.data.exception.HttpCodeException;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

public class HttpUtils {
    private HttpUtils() {
        throw new IllegalStateException("Utility class");
    }

    public static <T> T sendRequestForXMLAndParseToExpectedObject(String urlString, Class<T> responseType, RestTemplate restTemplate) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(List.of(MediaType.APPLICATION_XML));
        HttpEntity<String> entity = new HttpEntity<>("parameters", headers);
        ResponseEntity<T> response = restTemplate.exchange(urlString, HttpMethod.GET, entity, responseType);
        if (response.getStatusCode() != HttpStatus.OK) {
            throw new HttpCodeException(String.format(HttpCodeException.HTTP_CODE_EXCEPTION_MESSAGE, response.getStatusCode(), response.getBody()));
        }
        return response.getBody();
    }
}
