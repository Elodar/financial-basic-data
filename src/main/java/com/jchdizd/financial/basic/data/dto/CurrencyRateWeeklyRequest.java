package com.jchdizd.financial.basic.data.dto;

import com.jchdizd.financial.basic.data.validator.constraint.DateOnlySaturday;
import com.jchdizd.financial.basic.data.entity.BankSource;
import lombok.Data;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import java.time.LocalDate;
import java.util.List;

@Data
@ToString
public class CurrencyRateWeeklyRequest {

    @NotNull(message = "{validation.message.not.null}")
    @PastOrPresent(message = "{validation.message.date.not.future}")
    @DateOnlySaturday
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate requestDate;
    private List<BankSource> bankSource = List.of(BankSource.values());
}
