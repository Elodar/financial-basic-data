package com.jchdizd.financial.basic.data.dto.dnb;

import com.lppsa.integration.artifacts.accountancy.adapter.LocalDateAdapter;
import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(namespace = "http://www.ecb.int/vocabulary/2002-08-01/eurofxref")
public class Cube {

    @XmlAttribute(name = "time")
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    private LocalDate time;

    @XmlElement(name = "Cube", namespace = "http://www.ecb.int/vocabulary/2002-08-01/eurofxref")
    public List<SingleCube> currencies;
}
