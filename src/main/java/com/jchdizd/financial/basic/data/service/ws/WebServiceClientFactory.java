package com.jchdizd.financial.basic.data.service.ws;

import lombok.AllArgsConstructor;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebServiceException;
import java.net.URL;

@AllArgsConstructor
public class WebServiceClientFactory<T> {

    private final String wsdlUrl;
    private final QName serviceName;
    private final Class<T> serviceClass;

    public T createClient() {
        try {
            Service service = Service.create(new URL(wsdlUrl), serviceName);
            return service.getPort(serviceClass);
        } catch (Exception e) {
            throw new WebServiceException("Failed to create web service client", e);
        }
    }
}