package com.jchdizd.financial.basic.data.dto.bnr;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "DataSet")
@XmlAccessorType(XmlAccessType.FIELD)
public class BnrResponse {

    @XmlElement(name = "Header", namespace = "http://www.bnr.ro/xsd")
    private Header header;

    @XmlElement(name = "Body", namespace = "http://www.bnr.ro/xsd")
    private Body body;
}