package com.jchdizd.financial.basic.data.entity;

public enum CurrencyType {
    Z,
    Y,
    X,
    C,
    O,
    L,
    P,
    U,
    G
}
