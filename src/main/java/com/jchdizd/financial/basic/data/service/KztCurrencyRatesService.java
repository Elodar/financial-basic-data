package com.jchdizd.financial.basic.data.service;

import com.jchdizd.financial.basic.data.config.kzt.KztProperties;
import com.jchdizd.financial.basic.data.dto.kzt.ExchangeRateDTO;
import com.jchdizd.financial.basic.data.entity.CurrencyRates;
import com.jchdizd.financial.basic.data.mapper.KztCurrencyRatesMapper;
import com.jchdizd.financial.basic.data.utils.HttpUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Service
@RequiredArgsConstructor
public class KztCurrencyRatesService implements CurrencyRateServiceInterface {

    private final KztProperties kztProperties;
    private final RestTemplate restTemplate;
    private final KztCurrencyRatesMapper mapper;

    @Override
    public CurrencyRates getCurrencyRateByDate(LocalDate date) throws Exception {
        return map(getCurrencyFromBank(date));
    }

    public ExchangeRateDTO getCurrencyFromBank(LocalDate date){
        String formattedDate = date.format(DateTimeFormatter.ofPattern("dd.MM.yyyy"));
        return HttpUtils.sendRequestForXMLAndParseToExpectedObject(kztProperties.getBase().replace("{date}", formattedDate), ExchangeRateDTO.class, restTemplate);
    }

    private CurrencyRates map(ExchangeRateDTO exchangeRateDTO) {
        return CurrencyRates.builder()
                .publishDate(exchangeRateDTO.getDate())
                .originalCurrency("KZT")
                .exchangeRates(
                        exchangeRateDTO.getItems().stream()
                                .map(mapper::map)
                                .toList()
                ).build();
    }
}