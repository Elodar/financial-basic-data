package com.jchdizd.financial.basic.data.dto.bnr;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;
import java.math.BigDecimal;

@Data
@XmlAccessorType(XmlAccessType.FIELD)
public class Rate {

    @XmlAttribute(name = "currency")
    private String currency;

    @XmlAttribute(name = "multiplier")
    private int multiplier;

    @XmlValue
    private BigDecimal value;
}