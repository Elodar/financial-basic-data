package com.jchdizd.financial.basic.data.service;

import com.jchdizd.financial.basic.data.config.nbp.NbpProperties;
import com.jchdizd.financial.basic.data.mapper.NbpMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class NbpCurrencyRatesTableAService extends AbstractNbpCurrencyRatesService {

    public NbpCurrencyRatesTableAService(RestTemplate restTemplate, NbpMapper mapper, NbpProperties nbpConfiguration) {
        super(restTemplate, mapper, nbpConfiguration.getNbpUrlForTableA());
    }
}