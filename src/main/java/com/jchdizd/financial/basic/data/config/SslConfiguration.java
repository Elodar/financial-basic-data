package com.jchdizd.financial.basic.data.config;

import ch.qos.logback.core.net.ssl.KeyStoreFactoryBean;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.ws.transport.http.HttpComponentsMessageSender;

import javax.net.ssl.SSLContext;

@Configuration
@ConfigurationProperties("http.client.ssl")
@Data
@Slf4j
public class SslConfiguration {

    private Resource trustStore;

    private String trustStorePassword;

    private Resource keyStore;

    private String keyStorePassword;


    @Bean
    public KeyStoreFactoryBean keyStore() throws Exception {
        if (keyStore == null || StringUtils.isEmpty(keyStorePassword)) {
            log.warn("KeyStore is not configured properly");
            return null;
        }
        if (!keyStore.exists()) {
            log.info("File KeyStore not found" + keyStore.getURL());
            return null;
        }
        KeyStoreFactoryBean keyStoreFactoryBean = new KeyStoreFactoryBean();
        keyStoreFactoryBean.setLocation(keyStore.getURL().toString());
        keyStoreFactoryBean.setPassword(keyStorePassword);
        return keyStoreFactoryBean;
    }

    @Bean
    public KeyStoreFactoryBean trustStore() throws Exception {
        if (trustStore == null || StringUtils.isEmpty(trustStorePassword)) {
            log.warn("TrustStore is not configured properly");
            return null;
        }
        if (!trustStore.exists()) {
            log.info("File TrustStore not found" + trustStore.getURL());
            return null;
        }
        KeyStoreFactoryBean keyStoreFactoryBean = new KeyStoreFactoryBean();
        keyStoreFactoryBean.setLocation(trustStore.getURL().toString());
        keyStoreFactoryBean.setPassword(trustStorePassword);
        return keyStoreFactoryBean;
    }

    @Bean
    public HttpComponentsMessageSender httpComponentsMessageSender() throws Exception {

        KeyStoreFactoryBean keyStoreFactoryBean = keyStore();
        KeyStoreFactoryBean trustStoreFactoryBean = trustStore();
        if (keyStoreFactoryBean == null || trustStoreFactoryBean == null) {
            log.info("SSL is not configured properly");
            return new HttpComponentsMessageSender();
        }

        SSLContext sslContext = new SSLContextBuilder()
                .loadTrustMaterial(
                        trustStoreFactoryBean.createKeyStore(),
                        null
                )
                .loadKeyMaterial(
                        keyStoreFactoryBean.createKeyStore(),
                        keyStorePassword.toCharArray()
                )
                .build();
        SSLConnectionSocketFactory socketFactory =
                new SSLConnectionSocketFactory(sslContext);
        CloseableHttpClient httpClient = HttpClients
                .custom()
                .setSSLSocketFactory(socketFactory)
                .addInterceptorFirst(new HttpComponentsMessageSender.RemoveSoapHeadersInterceptor())
                .build();

        HttpComponentsMessageSender httpComponentsMessageSender = new HttpComponentsMessageSender();
        httpComponentsMessageSender.setHttpClient(httpClient);

        return httpComponentsMessageSender;
    }


}
