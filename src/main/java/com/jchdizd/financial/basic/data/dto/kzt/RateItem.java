package com.jchdizd.financial.basic.data.dto.kzt;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.math.BigDecimal;

@Data
@XmlAccessorType(XmlAccessType.FIELD)
public class RateItem {

    @XmlElement(name = "fullname")
    private String fullname;

    @XmlElement(name = "title")
    private String title;

    @XmlElement(name = "description")
    private BigDecimal description;

    @XmlElement(name = "quant")
    private String quant;

    @XmlElement(name = "index")
    private String index;

    @XmlElement(name = "change")
    private String change;
}