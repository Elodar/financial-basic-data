package com.jchdizd.financial.basic.data.dto.bnr;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@Data
@XmlAccessorType(XmlAccessType.FIELD)
public class Body {

    @XmlElement(name = "Subject", namespace = "http://www.bnr.ro/xsd")
    private String subject;

    @XmlElement(name = "OrigCurrency", namespace = "http://www.bnr.ro/xsd")
    private String origCurrency;

    @XmlElement(name = "Cube", namespace = "http://www.bnr.ro/xsd")
    private Cube cube;
}