package com.jchdizd.financial.basic.data.config.dnb;

import com.jchdizd.financial.basic.data.config.BankProperties;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@Configuration
@ConfigurationProperties("currency.dnb")
public class DnbProperties implements BankProperties {

    @NotNull
    private String base;
    private boolean enabled;
    private int publicationHour;
    private long rangeOfDays;
}