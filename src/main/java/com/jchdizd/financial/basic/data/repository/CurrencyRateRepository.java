package com.jchdizd.financial.basic.data.repository;

import com.jchdizd.financial.basic.data.entity.BankSource;
import com.jchdizd.financial.basic.data.entity.CurrencyRate;
import com.jchdizd.financial.basic.data.entity.CurrencyType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface CurrencyRateRepository extends JpaRepository<CurrencyRate, CurrencyRate.Pk> {
    List<CurrencyRate> findAllByEffectiveDateBetweenAndExchangeTypeAndBankSourceIn(LocalDate startEffectiveDate, LocalDate endEffectiveDate, CurrencyType exchangeType, List<BankSource> bankSources);

    List<CurrencyRate> findAllByEffectiveDateBetweenAndExchangeTypeAndBankSourceAndCurrencyCode(LocalDate startEffectiveDate, LocalDate endEffectiveDate, CurrencyType exchangeType, BankSource bankSource, String currencyCode);

    List<CurrencyRate> findAllByEffectiveDateBetweenAndCurrencyCodeAndCurrencyCodeFromAndBankSourceAndExchangeType(LocalDate startEffectiveDate, LocalDate endEffectiveDate, String currencyCode, String currencyCodeFrom, BankSource bankSource, CurrencyType exchangeType);

    Optional<CurrencyRate> findFirstByCurrencyCodeFromAndCurrencyCodeAndExchangeTypeAndEffectiveDateIsLessThanEqualOrderByEffectiveDateDesc(String currencyCodeFrom, String currencyCodeTo, CurrencyType exchangeType, LocalDate date);
}
