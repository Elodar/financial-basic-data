package com.jchdizd.financial.basic.data.service;

import com.jchdizd.financial.basic.data.entity.CurrencyRates;

import java.time.LocalDate;

public interface CurrencyRateServiceInterface {


    CurrencyRates getCurrencyRateByDate(LocalDate date) throws Exception;

}
