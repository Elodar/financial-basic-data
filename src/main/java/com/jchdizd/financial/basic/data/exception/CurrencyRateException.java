package com.jchdizd.financial.basic.data.exception;

public class CurrencyRateException extends RuntimeException {
    public static final String CURRENCY_NOT_FOUND_EXCEPTION_MESSAGE = "Currency rate not found for bankSource %s";

    public CurrencyRateException(String message) {
        super(message);
    }
}
