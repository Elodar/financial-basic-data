package com.jchdizd.financial.basic.data.service;

import com.jchdizd.financial.basic.data.dto.CurrencyRequest;
import com.jchdizd.financial.basic.data.dto.CurrencyResponse;
import com.jchdizd.financial.basic.data.entity.BankSource;
import com.jchdizd.financial.basic.data.entity.CurrenciesParams;
import com.jchdizd.financial.basic.data.entity.CurrencyRate;
import com.jchdizd.financial.basic.data.entity.CurrencyRates;
import com.jchdizd.financial.basic.data.entity.CurrencyType;
import com.jchdizd.financial.basic.data.entity.ExchangeRate;
import com.jchdizd.financial.basic.data.exception.CurrencyRateException;
import com.jchdizd.financial.basic.data.repository.CurrenciesParamsRepository;
import com.jchdizd.financial.basic.data.repository.CurrencyRateRepository;
import com.jchdizd.financial.basic.data.entity.*;
import com.jchdizd.financial.basic.data.mapper.ExchangeRateKmdToCurrencyRateMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
@Transactional
public class CurrencyRateService {
    private final CurrencyRateRepository currencyRateRepository;
    private final CurrenciesParamsRepository currenciesParamsRepository;
    private final ExchangeRateKmdToCurrencyRateMapper mapper;

    public void processCurrencyRates(CurrencyRates currencyRate) {
        List<CurrenciesParams> currenciesParams = currenciesParamsRepository.findAll();

        currencyRate
                .getExchangeRates()
                .stream()
                .filter(exchangeRate -> currenciesParams.stream().anyMatch(
                        currencyParam -> isExchangeRateMatchingAnyCurrencyParam(currencyRate, exchangeRate, currencyParam)
                ))
                .forEach(exchangeRate -> processExchangeRate(exchangeRate, currencyRate.getPublishDate(), currencyRate.getSourceId(), currencyRate.getOriginalCurrency()));
    }

    private boolean isExchangeRateMatchingAnyCurrencyParam(CurrencyRates currencyRate, ExchangeRate exchangeRate, CurrenciesParams currencyParam) {
        return currencyParam.getCurrencyCodeFrom().equals(currencyRate.getOriginalCurrency())
                && currencyParam.getCurrencyCodeTo().equals(exchangeRate.getCurrency())
                && currencyParam.getBankSource().equals(currencyRate.getSourceId());
    }

    private void processExchangeRate(ExchangeRate exchangeRate, LocalDate publishDate, BankSource sourceId, String originalCurrency) {
        CurrencyRate currencyRate = mapper.map(exchangeRate, publishDate, sourceId, originalCurrency, exchangeRate.getScale());
        CurrencyRate reversedCurrencyRate = mapper.reverseMap(exchangeRate, publishDate, sourceId, originalCurrency, exchangeRate.getScale());
        currencyRateRepository.save(currencyRate);
        log.info("CurrencyRate saved FROM {} to {} ExchangeRate {} BankSource {} CurrencyRate Date {} RateScaler {}", currencyRate.getCurrencyCodeFrom(),
                currencyRate.getCurrencyCode(), currencyRate.getExchangeRate(), currencyRate.getBankSource(), currencyRate.getEffectiveDate(), currencyRate.getScale());
        currencyRateRepository.save(reversedCurrencyRate);
        log.info("ReversedCurrencyRate saved FROM {} to {} ExchangeRate {} BankSource {} CurrencyRate Date {} RateScaler {}", reversedCurrencyRate.getCurrencyCodeFrom(),
                reversedCurrencyRate.getCurrencyCode(), reversedCurrencyRate.getExchangeRate(), reversedCurrencyRate.getBankSource(), reversedCurrencyRate.getEffectiveDate(), reversedCurrencyRate.getScale());
        createAverageCurrencyRateLast30(currencyRate.getCurrencyCode(), currencyRate.getCurrencyCodeFrom(), currencyRate.getBankSource(), publishDate);
        createAverageCurrencyRateLast30(reversedCurrencyRate.getCurrencyCode(), reversedCurrencyRate.getCurrencyCodeFrom(), reversedCurrencyRate.getBankSource(), publishDate);
        calculateAverageCurrencyRateForCurrencyBeforeLastSaturday(publishDate, currencyRate.getBankSource(), currencyRate);
    }

    private void calculateAverageCurrencyRateForCurrencyBeforeLastSaturday(LocalDate date, BankSource sourceBank, CurrencyRate currencyRate) {
        LocalDate lastSaturday = LocalDate.now().with(TemporalAdjusters.previous(DayOfWeek.SATURDAY));
        if (date.isBefore(lastSaturday)) {
            log.info("Calculating weekly average due to insertion of past currency rate. Saturday: {}, for bankSource {}",
                    date.with(TemporalAdjusters.next(DayOfWeek.SATURDAY)), sourceBank);
            LocalDate weekEndDate = date.with(TemporalAdjusters.nextOrSame(DayOfWeek.SATURDAY));
            LocalDate weekStartDate = weekEndDate.with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
            List<CurrencyRate> currencyRates = currencyRateRepository.findAllByEffectiveDateBetweenAndExchangeTypeAndBankSourceAndCurrencyCode(weekStartDate, weekEndDate, CurrencyType.Z, sourceBank, currencyRate.getCurrencyCode());
            processWeeklyCurrencyRatesPerBankSource(weekEndDate, sourceBank, currencyRates);
        }
    }

    private void createAverageCurrencyRateLast30(String currencyCode, String currencyCodeFrom, BankSource bankSource, LocalDate publishDate) {
        LocalDate beginDate = publishDate.minusDays(29);
        List<CurrencyRate> currencyRates = currencyRateRepository.findAllByEffectiveDateBetweenAndCurrencyCodeAndCurrencyCodeFromAndBankSourceAndExchangeType(beginDate, publishDate, currencyCode, currencyCodeFrom, bankSource, CurrencyType.Z);
        createAndSaveAverageCurrencyRate(currencyRates, publishDate, CurrencyType.C);
        createAndSaveAverageCurrencyRate(currencyRates, publishDate, CurrencyType.O);
    }

    public void processWeeklyCurrencyRates(LocalDate requestDate, List<BankSource> bankSources) {
        log.info("Calculate weekly currency rates START for date: {}", requestDate);
        LocalDate weekStartDate = requestDate.with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
        List<CurrencyRate> currencyRates = currencyRateRepository.findAllByEffectiveDateBetweenAndExchangeTypeAndBankSourceIn(weekStartDate, requestDate, CurrencyType.Z, bankSources);
        Map<BankSource, List<CurrencyRate>> currencyMap = currencyRates.stream().collect(Collectors.groupingBy(CurrencyRate::getBankSource));
        currencyMap.forEach((bank, currencies) -> processWeeklyCurrencyRatesPerBankSource(requestDate, bank, currencies));
        log.info("Calculate weekly currency rates FINISH");
    }

    private void processWeeklyCurrencyRatesPerBankSource(LocalDate requestDate, BankSource bankSource, List<CurrencyRate> currencyRates) {
        log.info("Calculate weekly currency rates for: {}", bankSource);
        Map<String, Map<String, List<CurrencyRate>>> ratesGroupedByCodeFromAndCodeTo = currencyRates.stream().collect(Collectors.groupingBy(
                CurrencyRate::getCurrencyCode,
                Collectors.groupingBy(CurrencyRate::getCurrencyCodeFrom)
        ));
        ratesGroupedByCodeFromAndCodeTo.forEach((currencyCodeTo, currenciesFrom) ->
                currenciesFrom.forEach((currencyFrom, currencies) ->
                        createAndSaveAverageCurrencyRate(currencies, requestDate, CurrencyType.Y)
                ));
    }

    private void createAndSaveAverageCurrencyRate(List<CurrencyRate> currencyRates, LocalDate publishDate, CurrencyType exchangeType) {
        CurrencyRate averageCurrencyRate = calculateAverageCurrencyRate(currencyRates);
        averageCurrencyRate.setExchangeType(exchangeType);
        averageCurrencyRate.setEffectiveDate(publishDate);
        currencyRateRepository.save(averageCurrencyRate);
        log.info("AverageCurrencyRate {} saved FROM {} to {} ExchangeRate {} BankSource {} CurrencyRate Date {} RateScaler {}", averageCurrencyRate.getExchangeType(), averageCurrencyRate.getCurrencyCodeFrom(),
                averageCurrencyRate.getCurrencyCode(), averageCurrencyRate.getExchangeRate(), averageCurrencyRate.getBankSource(), averageCurrencyRate.getEffectiveDate(), averageCurrencyRate.getScale());
    }

    private CurrencyRate calculateAverageCurrencyRate(List<CurrencyRate> currencyRates) {
        BigDecimal averageRateValue = currencyRates.stream()
                .map(CurrencyRate::getExchangeRate).reduce(BigDecimal.ZERO, BigDecimal::add)
                .divide(BigDecimal.valueOf(currencyRates.size()), 10, RoundingMode.HALF_EVEN);
        CurrencyRate averageCurrencyRate = new CurrencyRate();
        averageCurrencyRate.setCurrencyCode(currencyRates.get(0).getCurrencyCode());
        averageCurrencyRate.setScale(BigDecimal.ONE);
        averageCurrencyRate.setCurrencyCodeFrom(currencyRates.get(0).getCurrencyCodeFrom());
        averageCurrencyRate.setBankSource(currencyRates.get(0).getBankSource());
        averageCurrencyRate.setExchangeRate(averageRateValue);
        return averageCurrencyRate;
    }

    public CurrencyResponse getCurrencyRate(CurrencyRequest currencyRequest) {
        Optional<CurrencyRate> currencyRateOptional = currencyRateRepository.
                findFirstByCurrencyCodeFromAndCurrencyCodeAndExchangeTypeAndEffectiveDateIsLessThanEqualOrderByEffectiveDateDesc(
                        currencyRequest.getCurrencyFrom(),
                        currencyRequest.getCurrencyTo(),
                        currencyRequest.getCurrencyType(),
                        currencyRequest.getDate()
                );
        if (currencyRateOptional.isPresent()) {
            return CurrencyRate.fromCurrencyRate(currencyRateOptional.get());
        } else {
            throw new CurrencyRateException(String.format(CurrencyRateException.CURRENCY_NOT_FOUND_EXCEPTION_MESSAGE, currencyRequest.getBankSource()));
        }
    }
}
