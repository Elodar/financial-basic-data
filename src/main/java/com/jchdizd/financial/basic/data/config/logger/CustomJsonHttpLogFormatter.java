package com.jchdizd.financial.basic.data.config.logger;

import lombok.RequiredArgsConstructor;
import org.zalando.logbook.Correlation;
import org.zalando.logbook.HttpLogFormatter;
import org.zalando.logbook.HttpRequest;
import org.zalando.logbook.HttpResponse;
import org.zalando.logbook.Precorrelation;
import org.zalando.logbook.json.JsonHttpLogFormatter;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.Map;

@RequiredArgsConstructor
public class CustomJsonHttpLogFormatter implements HttpLogFormatter {

    private final JsonHttpLogFormatter delegate;

    @Override
    public String format(@NotNull Precorrelation precorrelation, @NotNull HttpRequest request) throws IOException {
        final Map<String, Object> content = delegate.prepare(precorrelation, request);
        content.remove("remote");
        content.remove("origin");
        content.remove("protocol");
        content.remove("host");
        content.remove("path");
        content.remove("scheme");
        content.remove("port");
        content.remove("headers");
        content.remove("Date");
        content.put("type", "REQUEST");
        return delegate.format(content);
    }

    @Override
    public String format(@NotNull Correlation correlation, @NotNull HttpResponse response) throws IOException {
        final Map<String, Object> content = delegate.prepare(correlation, response);
        content.remove("origin");
        content.put("type", "RESPONSE");
        content.remove("protocol");
        content.remove("headers");
        return delegate.format(content);
    }
}
