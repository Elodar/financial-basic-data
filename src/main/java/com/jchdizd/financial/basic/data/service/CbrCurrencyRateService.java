package com.jchdizd.financial.basic.data.service;

import com.jchdizd.financial.basic.data.exception.DifferentDatesException;
import com.jchdizd.financial.basic.data.config.cbr.CbrProperties;
import com.jchdizd.financial.basic.data.dto.cbr.ValCurs;
import com.jchdizd.financial.basic.data.entity.BankSource;
import com.jchdizd.financial.basic.data.entity.CurrencyRates;
import com.jchdizd.financial.basic.data.mapper.CbrCurrencyRatesMapper;
import com.jchdizd.financial.basic.data.utils.HttpUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Service
@RequiredArgsConstructor
public class CbrCurrencyRateService implements CurrencyRateServiceInterface {

    private final CbrProperties cbrProperties;
    private final RestTemplate restTemplate;
    private final CbrCurrencyRatesMapper mapper;

    public static final String DATE_FORMAT_FOR_CBR_PATH_VARIABLE = "dd/MM/yyyy";

    @Override
    public CurrencyRates getCurrencyRateByDate(LocalDate date) throws Exception {
        return map(getCurrencyValuesByDate(date));
    }

    private String prepareUrlStringWithAssignedDate(LocalDate dateToFind) {
        String formattedDate = dateToFind.format(DateTimeFormatter.ofPattern(DATE_FORMAT_FOR_CBR_PATH_VARIABLE));
        return cbrProperties.getBase() + String.format(cbrProperties.getPath(), formattedDate);
    }

    public ValCurs getCurrencyValuesByDate(LocalDate date) {
        String urlString = prepareUrlStringWithAssignedDate(date);
        ValCurs valCurs = HttpUtils.sendRequestForXMLAndParseToExpectedObject(urlString, ValCurs.class, restTemplate);
        if (!valCurs.getDate().equals(date)) {
            throw new DifferentDatesException(String.format(DifferentDatesException.DIFFERENT_DATES_EXCEPTION_MESSAGE, BankSource.CBR, valCurs.getDate(), date));
        }
        return valCurs;
    }

    private CurrencyRates map(ValCurs valCurs) {
        return CurrencyRates.builder()
                .publishDate(valCurs.getDate())
                .originalCurrency("RUB")
                .exchangeRates(
                        valCurs.getValutes().stream()
                                .map(mapper::map)
                                .toList()
                ).build();
    }
}
