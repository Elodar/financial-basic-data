package com.jchdizd.financial.basic.data.dto.nbrm;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Getter
@Setter
@XmlRootElement(name = "dsKurs")
@XmlAccessorType(XmlAccessType.FIELD)
public class DsKurs {

    @XmlElement(name = "KursZbir")
    private List<KursZbir> kursZbir;
}