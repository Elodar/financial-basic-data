package com.jchdizd.financial.basic.data.controller;

import com.jchdizd.financial.basic.data.dto.CurrencyRequest;
import com.jchdizd.financial.basic.data.dto.CurrencyResponse;
import com.jchdizd.financial.basic.data.entity.BankSource;
import com.jchdizd.financial.basic.data.entity.CurrencyRates;
import com.jchdizd.financial.basic.data.entity.CurrencyType;
import com.jchdizd.financial.basic.data.service.CurrencyRateService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

@RestController
@RequestMapping(CurrencyRateController.URI_BASE)
@Api("Currency rates controller")
@RequiredArgsConstructor
@Slf4j
public class CurrencyRateController {
    public static final String URI_BASE = "/currency";

    private final CurrencyRateService currencyRateService;

    @GetMapping
    public ResponseEntity<CurrencyResponse> getCurrencyRate(
            @RequestParam("from") @NotBlank String currencyCodeFrom,
            @RequestParam("to") @NotBlank String currencyCodeTo,
            @RequestParam("date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date,
            @RequestParam(name = "type", required = false, defaultValue = "Z") CurrencyType currencyType
    ) {
        CurrencyResponse currencyResponse = currencyRateService.getCurrencyRate(CurrencyRequest.createCurrencyDTO(currencyCodeFrom, currencyCodeTo, date, currencyType));
        return ResponseEntity.status(HttpStatus.OK).body(currencyResponse);
    }

    @ApiIgnore
    @PostMapping
    public void processCurrencyRates(@RequestBody CurrencyRates currencyRate) {
        currencyRateService.processCurrencyRates(currencyRate);
    }
}
