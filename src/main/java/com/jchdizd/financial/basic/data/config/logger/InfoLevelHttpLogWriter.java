package com.jchdizd.financial.basic.data.config.logger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.zalando.logbook.Correlation;
import org.zalando.logbook.HttpLogWriter;
import org.zalando.logbook.Logbook;
import org.zalando.logbook.Precorrelation;

import javax.validation.constraints.NotNull;

@Component
public class InfoLevelHttpLogWriter implements HttpLogWriter {

    private static final Logger log = LoggerFactory.getLogger(Logbook.class);

    @Override
    public boolean isActive() {

        return log.isInfoEnabled();
    }

    @Override
    public void write(final @NotNull Precorrelation precorrelation, final @NotNull String request) {
        log.info(request);
    }

    @Override
    public void write(final @NotNull Correlation correlation, final @NotNull String response) {
        log.info(response);
    }

}

