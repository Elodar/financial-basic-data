package com.jchdizd.financial.basic.data.service;

import com.jchdizd.financial.basic.data.config.cbr.CbrProperties;
import com.jchdizd.financial.basic.data.config.BankProperties;
import com.jchdizd.financial.basic.data.config.bnr.BnrProperties;
import com.jchdizd.financial.basic.data.config.byn.BynProperties;
import com.jchdizd.financial.basic.data.config.dnb.DnbProperties;
import com.jchdizd.financial.basic.data.config.ecb.EcbProperties;
import com.jchdizd.financial.basic.data.config.kzt.KztProperties;
import com.jchdizd.financial.basic.data.config.nbp.NbpProperties;
import com.jchdizd.financial.basic.data.config.nbrm.NbrmProperties;
import com.jchdizd.financial.basic.data.config.srb.SrbProperties;
import com.jchdizd.financial.basic.data.entity.BankSource;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class CurrencyRateProperties {

    private NbpProperties nbpProperties;
    private BnrProperties bnrProperties;
    private EcbProperties ecbProperties;
    private DnbProperties dnbProperties;
    private KztProperties kztProperties;
    private BynProperties bynProperties;
    private CbrProperties cbrProperties;
    private NbrmProperties nbrmProperties;
    private SrbProperties srbProperties;

    public BankProperties getProperties(BankSource bankSource) throws Exception {
        switch (bankSource) {
            case NBP_A, NBP_B -> {
                return nbpProperties;
            }
            case BNR -> {
                return bnrProperties;
            }
            case ECB -> {
                return ecbProperties;
            }
            case DNB -> {
                return dnbProperties;
            }
            case KZT -> {
                return kztProperties;
            }
            case BYN -> {
                return bynProperties;
            }
            case CBR -> {
                return cbrProperties;
            }
            case NBRM -> {
                return nbrmProperties;
            }
            case SRB -> {
                return srbProperties;
            }
        }
        throw new Exception("Unknown bank source");
    }
}