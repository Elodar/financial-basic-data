package com.jchdizd.financial.basic.data.mapper;

import com.jchdizd.financial.basic.data.dto.nbp.Rate;
import com.jchdizd.financial.basic.data.entity.ExchangeRate;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface NbpMapper {

    @Mapping(target = "scale", constant = "1")
    @Mapping(target = "currency", source = "code")
    @Mapping(target = "currencyName", source = "currency")
    @Mapping(target = "rate", source = "mid")
    ExchangeRate map(Rate rate);

}
