package com.jchdizd.financial.basic.data.service;

import com.jchdizd.financial.basic.data.entity.BankSource;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class BankService {

    private NbpCurrencyRatesTableAService nbpCurrencyRatesTableAService;
    private NbpCurrencyRatesTableBService nbpCurrencyRatesTableBService;
    private BnrCurrencyRatesService bnrCurrencyRatesService;
    private EcbCurrencyRatesService ecbCurrencyRatesService;
    private DnbCurrencyRateService dnbCurrencyRateService;
    private KztCurrencyRatesService kztCurrencyRatesService;
    private BynCurrencyFromBelarusService bynCurrencyFromBelarusService;
    private CbrCurrencyRateService cbrCurrencyRateService;
    private NbrmCurrencyRateService nbrmCurrencyRateService;
    private SrbCurrencyRateService srbCurrencyRateService;

    public CurrencyRateServiceInterface getBankService(BankSource bankSource) {
        return switch (bankSource) {
            case NBP_A -> nbpCurrencyRatesTableAService;
            case NBP_B -> nbpCurrencyRatesTableBService;
            case BNR -> bnrCurrencyRatesService;
            case ECB -> ecbCurrencyRatesService;
            case DNB -> dnbCurrencyRateService;
            case SRB -> srbCurrencyRateService;
            case KZT -> kztCurrencyRatesService;
            case BYN -> bynCurrencyFromBelarusService;
            case CBR -> cbrCurrencyRateService;
            case NBRM -> nbrmCurrencyRateService;
        };
    }
}