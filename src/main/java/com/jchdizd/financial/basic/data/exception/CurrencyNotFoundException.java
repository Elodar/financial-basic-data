package com.jchdizd.financial.basic.data.exception;

public class CurrencyNotFoundException extends RuntimeException {
    public static final String CURRENCY_NOT_FOUND_MESSAGE = "Currency not found for date %s";

    public CurrencyNotFoundException(String message) {
        super(message);
    }
}
