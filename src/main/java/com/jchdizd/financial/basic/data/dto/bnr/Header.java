package com.jchdizd.financial.basic.data.dto.bnr;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.util.Date;

@Data
@XmlAccessorType(XmlAccessType.FIELD)
public class Header {

    @XmlElement(name = "Publisher", namespace = "http://www.bnr.ro/xsd")
    private String publisher;

    @XmlElement(name = "PublishingDate", namespace = "http://www.bnr.ro/xsd")
    private Date publishingDate;

    @XmlElement(name = "MessageType", namespace = "http://www.bnr.ro/xsd")
    private String messageType;
}