package com.jchdizd.financial.basic.data.dto.nbrm;

import com.jchdizd.financial.basic.data.adapter.GregorianCalendarToLocalDateAdapter;
import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@XmlRootElement(name = "KursZbir")
@XmlAccessorType(XmlAccessType.FIELD)
public class KursZbir {

    @XmlElement(name = "RBr")
    private Double rBr;

    @XmlElement(name = "Datum")
    @XmlJavaTypeAdapter(GregorianCalendarToLocalDateAdapter.class)
    private LocalDate datum;

    @XmlElement(name = "Valuta")
    private Double valuta;

    @XmlElement(name = "Oznaka")
    private String oznaka;

    @XmlElement(name = "Drzava")
    private String drzava;

    @XmlElement(name = "Nomin")
    private Double nomin;

    @XmlElement(name = "Kupoven")
    private BigDecimal kupoven;

    @XmlElement(name = "Sreden")
    private BigDecimal sreden;

    @XmlElement(name = "Prodazen")
    private BigDecimal prodazen;

    @XmlElement(name = "DrzavaAng")
    private String drzavaAng;

    @XmlElement(name = "DrzavaAl")
    private String drzavaAl;

    @XmlElement(name = "NazivMak")
    private String nazivMak;

    @XmlElement(name = "NazivAng")
    private String nazivAng;

    @XmlElement(name = "ValutaNaziv_AL")
    private String valutaNazivAL;

    @XmlElement(name = "Datum_f")
    @XmlJavaTypeAdapter(GregorianCalendarToLocalDateAdapter.class)
    private LocalDate datumF;
}
