package com.jchdizd.financial.basic.data.service;

import com.jchdizd.financial.basic.data.config.ecb.EcbProperties;
import com.jchdizd.financial.basic.data.dto.ecb.Cube;
import com.jchdizd.financial.basic.data.dto.ecb.CubeList;
import com.jchdizd.financial.basic.data.dto.ecb.ResponeECB;
import com.jchdizd.financial.basic.data.exception.CurrencyRateException;
import com.jchdizd.financial.basic.data.mapper.EcbCurrencyRatesMapper;
import com.jchdizd.financial.basic.data.entity.CurrencyRates;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXB;
import java.net.URL;
import java.time.LocalDate;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class EcbCurrencyRatesService implements CurrencyRateServiceInterface {

    private final EcbProperties ecbProperties;
    private final EcbCurrencyRatesMapper mapper;

    @Override
    public CurrencyRates getCurrencyRateByDate(LocalDate date) throws Exception {
        return map(getCubesByDate(date));
    }

    public CubeList getCubesByDate(LocalDate date) throws Exception {

        ResponeECB cubes = JAXB.unmarshal(new URL(ecbProperties.getBase()), ResponeECB.class);
        List<Cube> filteredCubes = cubes.getCubeList().getCubesList().stream()
                .filter(cube -> cube.getDateOfCurrencyRates().equals(date))
                .toList();
        if (filteredCubes.isEmpty()) {
            throw new CurrencyRateException("Date is not available");
        }

        log.info("Returning {} cubes for date {}", filteredCubes.size(), date);

        return new CubeList(filteredCubes);
    }

    private CurrencyRates map(CubeList cubeList) {
        Cube cube = cubeList.getCubesList().get(0);

        return CurrencyRates.builder()
                .publishDate(cubeList.getCubesList().get(0).getDateOfCurrencyRates())
                .originalCurrency("EUR")
                .exchangeRates(
                        cube.getCubeCurrencyRateList().stream()
                                .map(mapper::map)
                                .toList()
                ).build();
    }
}