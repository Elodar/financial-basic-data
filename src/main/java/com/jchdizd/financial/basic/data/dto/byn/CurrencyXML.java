package com.jchdizd.financial.basic.data.dto.byn;

import com.jchdizd.financial.basic.data.adapter.CommaToDotAdapter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@XmlRootElement(name = "Currency")
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
public class CurrencyXML {

    @XmlElement(name = "NumCode")
    private String numCode;

    @XmlElement(name = "CharCode")
    private String charCode;

    @XmlElement(name = "Scale")
    private String scale;

    @XmlElement(name = "Name")
    private String name;

    @XmlElement(name = "Rate")
    @XmlJavaTypeAdapter(CommaToDotAdapter.class)
    private BigDecimal rate;

    @XmlElement(name = "Id")
    private String id;
}
