package com.jchdizd.financial.basic.data.config.srb;

import com.jchdizd.financial.basic.data.config.BankProperties;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@Configuration
@ConfigurationProperties("currency.srb")
public class SrbProperties implements BankProperties {

    @NotNull
    private String wsdlUrl;
    private ServiceProperties service;
    private boolean enabled;
    private int publicationHour;
    private long rangeOfDays;

    @Getter
    @Setter
    public static class ServiceProperties {
        @NotNull
        private String namespace;
        @NotNull
        private String localPart;
    }
}
