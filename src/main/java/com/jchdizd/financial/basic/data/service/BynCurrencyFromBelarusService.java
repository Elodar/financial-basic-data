package com.jchdizd.financial.basic.data.service;

import com.jchdizd.financial.basic.data.dto.byn.DailyExRates;
import com.jchdizd.financial.basic.data.entity.CurrencyRates;
import com.jchdizd.financial.basic.data.mapper.BynCurrencyRatesMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;
import java.time.LocalDate;

@Service
@RequiredArgsConstructor
@Slf4j
public class BynCurrencyFromBelarusService implements CurrencyRateServiceInterface{

    private final BynFtpService bynFtpService;
    private final BynCurrencyRatesMapper mapper;

    @Override
    public CurrencyRates getCurrencyRateByDate(LocalDate date) throws Exception {
        return map(downloadExRatesFromFtp(date));
    }

    public DailyExRates downloadExRatesFromFtp(LocalDate date) throws JAXBException, IOException {
        log.info("Downloading Belarus Currencies from FTP for: {}", date);
        JAXBContext jaxbContext = JAXBContext.newInstance(DailyExRates.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        return (DailyExRates) jaxbUnmarshaller.unmarshal(bynFtpService.searchFileInFTP(date));
    }

    private CurrencyRates map(DailyExRates dailyExRates) {
        return CurrencyRates.builder()
                .publishDate(dailyExRates.getDate())
                .originalCurrency("BYN")
                .exchangeRates(
                        dailyExRates.getCurrency().stream()
                                .map(mapper::map)
                                .toList()
                ).build();
    }
}