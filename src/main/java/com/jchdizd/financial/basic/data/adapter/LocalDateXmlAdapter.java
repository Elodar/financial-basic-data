package com.jchdizd.financial.basic.data.adapter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javax.xml.bind.annotation.adapters.XmlAdapter;

public class LocalDateXmlAdapter extends XmlAdapter<String, LocalDate> {

    private final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");

    @Override
    public LocalDate unmarshal(String value) throws Exception {
        return LocalDate.parse(value, dateFormatter);
    }

    @Override
    public String marshal(LocalDate value) throws Exception {
        return value.format(dateFormatter);
    }
}