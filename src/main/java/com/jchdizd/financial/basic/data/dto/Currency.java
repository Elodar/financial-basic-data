package com.jchdizd.financial.basic.data.dto;

import com.jchdizd.financial.basic.data.entity.BankSource;
import com.jchdizd.financial.basic.data.entity.CurrencyType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;

@Getter
@Setter
@ToString
@SuperBuilder
public abstract class Currency {

    private String currencyFrom;
    private String currencyTo;
    private LocalDate date;
    private BankSource bankSource;
    private CurrencyType currencyType;

}
