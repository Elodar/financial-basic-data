package com.jchdizd.financial.basic.data.repository;

import com.jchdizd.financial.basic.data.entity.BankSource;
import com.jchdizd.financial.basic.data.entity.DownloadCurrencyRateStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

@Repository
public interface DownloadCurrencyRateStatusRepository extends JpaRepository<DownloadCurrencyRateStatus, DownloadCurrencyRateStatus.Pk> {

    DownloadCurrencyRateStatus getDownloadCurrencyRateStatusByBankSourceAndEffectiveDate(BankSource bankSource, LocalDate effectiveDate);
}
