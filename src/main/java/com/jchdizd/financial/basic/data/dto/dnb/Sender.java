package com.jchdizd.financial.basic.data.dto.dnb;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
public class Sender {
    @XmlElement(name = "name", namespace = "http://www.gesmes.org/xml/2002-08-01")
    private String name;
}
