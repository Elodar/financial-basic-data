package com.jchdizd.financial.basic.data.mapper;


import com.jchdizd.financial.basic.data.dto.kzt.RateItem;
import com.jchdizd.financial.basic.data.entity.ExchangeRate;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface KztCurrencyRatesMapper {

    @Mapping(target = "scale", constant = "1")
    @Mapping(target = "currency", source = "title")
    @Mapping(target = "currencyName", source = "fullname")
    @Mapping(target = "rate", source = "description")
    ExchangeRate map(RateItem rateItem);
}