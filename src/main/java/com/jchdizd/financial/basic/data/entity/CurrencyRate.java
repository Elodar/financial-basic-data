package com.jchdizd.financial.basic.data.entity;

import com.jchdizd.financial.basic.data.dto.CurrencyResponse;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "CURRENCY_RATES", schema = "CURRENCY")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@IdClass(CurrencyRate.Pk.class)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class CurrencyRate {

    @Id
    @Column(name = "CURRENCY_CODE", nullable = false, length = 3)
    @EqualsAndHashCode.Include
    private String currencyCode;

    @Id
    @Column(name = "EFFECTIVE_DATE", nullable = false)
    @EqualsAndHashCode.Include
    private LocalDate effectiveDate;

    @Id
    @Column(name = "EXCHANGE_TYPE", nullable = false, length = 1)
    @EqualsAndHashCode.Include
    @Enumerated(EnumType.STRING)
    private CurrencyType exchangeType;

    @Column(name = "EXCHANGE_RATE", nullable = false, scale = 10, precision = 32)
    private BigDecimal exchangeRate;

    @Column(name = "RATE_SCALER", precision = 10)
    private BigDecimal scale;

    @Id
    @Column(name = "CURRENCY_CODE_FROM")
    @EqualsAndHashCode.Include
    private String currencyCodeFrom;

    @Id
    @Column(name = "BANK_SOURCE")
    @EqualsAndHashCode.Include
    @Enumerated(EnumType.STRING)
    private BankSource bankSource;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Pk implements Serializable {
        private String currencyCode;
        private LocalDate effectiveDate;
        private CurrencyType exchangeType;
        private String currencyCodeFrom;
        private BankSource bankSource;
    }

    public static CurrencyResponse fromCurrencyRate(CurrencyRate currencyRate) {
        return CurrencyResponse.builder()
                .currencyFrom(currencyRate.getCurrencyCodeFrom())
                .currencyTo(currencyRate.getCurrencyCode())
                .bankSource(currencyRate.getBankSource())
                .date(currencyRate.getEffectiveDate())
                .currencyType(currencyRate.getExchangeType())
                .exchangeRate(currencyRate.getExchangeRate())
                .build();
    }

}
