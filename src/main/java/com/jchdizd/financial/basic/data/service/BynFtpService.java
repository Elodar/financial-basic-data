package com.jchdizd.financial.basic.data.service;

import com.jchdizd.financial.basic.data.exception.CurrencyNotFoundException;
import com.jchdizd.financial.basic.data.config.byn.BynProperties;
import com.jchdizd.financial.basic.data.utils.BynFtpClient;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BynFtpService {
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("uuuu_MM_dd");
    private static final String FILE_EXTENSION = ".txt";

    private final BynProperties bynFtpProperties;

    public InputStream searchFileInFTP(LocalDate date) throws IOException {
        InputStream fileFromFTP;

        String fileName = date.format(DATE_TIME_FORMATTER) + FILE_EXTENSION;
        String path = bynFtpProperties.getPath();

        try (BynFtpClient bynFtpClient = new BynFtpClient(bynFtpProperties)) {
            bynFtpClient.openConnection();
            List<String> fileNames = bynFtpClient.listFiles(path);
            if (fileNames.contains(fileName)) {
                fileFromFTP = bynFtpClient.getFileFromFTP(path + fileName);
            } else {
                throw new CurrencyNotFoundException(String.format(CurrencyNotFoundException.CURRENCY_NOT_FOUND_MESSAGE, date));
            }
            return fileFromFTP;
        }
    }
}
