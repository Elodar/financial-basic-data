package com.jchdizd.financial.basic.data.mapper;

import com.jchdizd.financial.basic.data.dto.cbr.Valute;
import com.jchdizd.financial.basic.data.entity.ExchangeRate;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface CbrCurrencyRatesMapper {

    @Mapping(target = "scale",source = "nominal")
    @Mapping(target = "currencyName", source = "name")
    @Mapping(target = "currency", source = "charCode")
    @Mapping(target = "rate", source = "value")
    ExchangeRate map(Valute rate);
}
