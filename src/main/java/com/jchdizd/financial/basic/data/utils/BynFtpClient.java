package com.jchdizd.financial.basic.data.utils;

import com.jchdizd.financial.basic.data.config.byn.BynProperties;
import lombok.RequiredArgsConstructor;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

@Component
@RequiredArgsConstructor
public class BynFtpClient implements AutoCloseable {
    private final BynProperties bynFtpProperties;
    private FTPClient ftp;

    public void openConnection() throws IOException {
        ftp = new FTPClient();

        ftp.connect(bynFtpProperties.getServer(), bynFtpProperties.getPort());

        int reply = ftp.getReplyCode();
        if (!FTPReply.isPositiveCompletion(reply)) {
            ftp.disconnect();
            throw new IOException("Exception in connecting to FTP Server");
        }
        ftp.login(bynFtpProperties.getUser(), bynFtpProperties.getPassword());
        ftp.enterLocalPassiveMode();
    }

    @Override
    public void close() throws IOException {
        ftp.disconnect();
    }

    public InputStream getFileFromFTP(String source) throws IOException {
        return ftp.retrieveFileStream(source);
    }

    public List<String> listFiles(String path) throws IOException {
        FTPFile[] files = ftp.listFiles(path);
        return Arrays.stream(files).map(FTPFile::getName).toList();

    }
}
