package com.jchdizd.financial.basic.data.config.kzt;

import com.jchdizd.financial.basic.data.config.BankProperties;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@Configuration
@ConfigurationProperties("currency.kzt")
public class KztProperties implements BankProperties {

    @NotNull
    private String base;
    private boolean enabled;
    private int publicationHour;
    private long rangeOfDays;
}