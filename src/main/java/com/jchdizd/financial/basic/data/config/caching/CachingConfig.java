package com.jchdizd.financial.basic.data.config.caching;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.Objects;

@Configuration
@EnableCaching
@EnableScheduling
@Slf4j
public class CachingConfig {

    private static final String CACHE_CURRENCY_PARAMS = "currencyParams";

    @Bean
    public CacheManager cacheManager() {
        return new ConcurrentMapCacheManager(CACHE_CURRENCY_PARAMS);
    }

    @Scheduled(cron = "0 0 * * * ?")
    public void clearAllCachesInOneHour() {
        log.info("All caches cleared");
        cacheManager().getCacheNames().stream().filter(Objects::nonNull).forEach(
                cacheName -> cacheManager().getCache(cacheName).clear());
    }
}
