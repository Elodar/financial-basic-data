package com.jchdizd.financial.basic.data.dto.byn;

import com.jchdizd.financial.basic.data.adapter.LocalDateXmlAdapter;
import lombok.Data;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDate;
import java.util.List;

@Data
@XmlRootElement(name = "DailyExRates")
@XmlAccessorType(XmlAccessType.FIELD)
public class DailyExRates {

    @XmlElement(name = "Currency")
    private List<CurrencyXML> currency;

    @XmlAttribute(name = "Date")
    @XmlJavaTypeAdapter(LocalDateXmlAdapter.class)
    private LocalDate date;
}
